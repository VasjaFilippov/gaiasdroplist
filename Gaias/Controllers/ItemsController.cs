﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Web.Mvc;
using Gaias.ActionFilters;
using Gaias.Db;
using Gaias.Db.Entities;
using Gaias.Dtos;
using Gaias.Enums;
using Gaias.Filters;
using Gaias.Models;
using Gaias.Sorters;
using Gaias.Utilities;

namespace Gaias.Controllers
{
    public class ItemsController : Controller
    {
        public static readonly ItemSorter Sorter = new ItemSorter();
        public static readonly ItemFilter Filter = new ItemFilter();
        
        [HttpPost, JsonNetFilter]
        public JsonResult Table(TableModel parameters)
        {
            using (var db = new Context())
            {
                int total, pageNumber;
                var data = db.Items.GenerateTable(
                    parameters: parameters,
                    inToDto: EntitiesToDto,
                    dtoToModel: dto => new ItemModel(dto),
                    filter: Filter,
                    sorter: Sorter,
                    defaultFilters: new FilterData[0],
                    defaultSorts: new[] { new Sorting("Name", true), },
                    totalPages: out total,
                    pageNumber: out pageNumber);

                return Json(new
                {
                    Rows = data,
                    Total = total,
                    Page = pageNumber,
                });
            }
        }

        [HttpPost, JsonNetFilter]
        public JsonResult ItemTypes()
        {
            using (var db = new Context())
            {
                var data = db.ItemTypes
                    .Select(it => new ItemTypeModel
                    {
                        Id = it.Id,
                        Name = it.Name,
                    });

                return Json(new
                {
                    Rows = data.ToArray(),
                });
            }
        }

        [HttpPost, JsonNetFilter]
        public JsonResult ItemRarities()
        {
            using (var db = new Context())
            {
                var data = db.ItemColors
                    .Select(it => new ItemColorModel
                    {
                        Id = it.Id,
                        Name = it.Name,
                        Color = it.Color,
                    });

                return Json(new
                {
                    Rows = data.ToArray(),
                });
            }
        }

        [HttpPost, JsonNetFilter]
        public JsonResult RemoveItem(RequestEntityModel model)
        {
            var claimsIdentity = (ClaimsIdentity)HttpContext.User.Identity;

            if (!claimsIdentity.ValidateRights(UserRights.RemoveEntities))
            {
                return Json(new
                {
                    Result = false,
                    Errors = new[]
                    {
                        new ErrorModel
                        {
                            Code = ErrorCodes.NotEnoughRights,
                            Description = "You don't have rights to remove entities",
                            AdditionalInfo = new
                            {
                                RequiredRights = UserRights.RemoveEntities,
                            }
                        },
                    },
                });
            }

            using (var db = new Context())
            {
                var item = db.Items.FirstOrDefault(it => it.Id == model.Id);

                if (item == null)
                {
                    return Json(new
                    {
                        Result = false,
                        Errors = new[]
                        {
                            new ErrorModel
                            {
                                Code = ErrorCodes.EntityNotFound,
                                Description = "Entity not found",
                            },
                        },
                    });
                }

                db.Items.Remove(item);

                return Json(new
                {
                    Result = true,
                });
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        private static Expression<Func<Item, ItemDto>> EntitiesToDto { get; } = it => new ItemDto
        {
            Id = it.Id,
            Name = it.Name,
            TypeId = it.Type.Id,
            Type = it.Type.Name,
            Rarity = it.Color.Name,
            RarityId = it.Color.Id,
            Color = it.Color.Color,
            Bonuses = it.Bonuses
                .Select(b => new BonusDto
                {
                    Stat = b.Bonus.Stat.Name,
                    Value = b.Bonus.Value,
                }),
            Creeps = it.Drops
                .Select(c => new CreepDto
                {
                    Name = c.Creep.Name,
                    Level = c.Creep.Level,
                }),
        };
    }
}