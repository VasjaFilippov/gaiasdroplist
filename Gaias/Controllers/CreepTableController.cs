﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Gaias.Db;
using Gaias.Db.Entities;
using Gaias.Dtos;
using Gaias.Filters;
using Gaias.Models;
using Gaias.Sorters;
using Gaias.Utilities;

namespace Gaias.Controllers
{
    public class CreepTableController : Controller
    {
        public static readonly ItemSorter Sorter = new ItemSorter();
        public static readonly ItemFilter Filter = new ItemFilter();

        [HttpPost]
        public JsonResult Table(TableModel parameters)
        {
            using (var db = new Context())
            {
                int page, pages;
                var data = db.Items.GenerateTable(
                    parameters: parameters,
                    inToDto: EntitiesToDto,
                    dtoToModel: dto => new ItemModel(dto),
                    filter: Filter,
                    sorter: Sorter,
                    defaultFilters: new FilterData[0],
                    defaultSorts: new[] { new Sorting("Name", true), },
                    totalPages: out pages,
                    pageNumber: out page);

                return Json(new
                {
                    Rows = data,
                    Page = page,
                    Total = pages,
                });
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        private static Expression<Func<Item, ItemDto>> EntitiesToDto { get; } = it => new ItemDto
        {
            Id = it.Id,
            Name = it.Name,
            TypeId = it.Type.Id,
            Type = it.Type.Name,
            Rarity = it.Color.Name,
            RarityId = it.Color.Id,
            Color = it.Color.Color,
            Bonuses = it.Bonuses
                .Select(b => new BonusDto
                {
                    Stat = b.Bonus.Stat.Name,
                    Value = b.Bonus.Value,
                }),
            Creeps = it.Drops
                .Select(c => new CreepDto
                {
                    Name = c.Creep.Name,
                    Level = c.Creep.Level,
                }),
        };
    }
}