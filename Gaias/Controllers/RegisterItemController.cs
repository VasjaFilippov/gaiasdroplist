﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;
using Gaias.ActionFilters;
using Gaias.Db;
using Gaias.Db.Entities;
using Gaias.Enums;
using Gaias.Models;
using Gaias.Utilities;
using Microsoft.AspNet.Identity;

namespace Gaias.Controllers
{
    [Authorize]
    public class RegisterItemController : Controller
    {
        // GET: RegisterItem
        public ActionResult OldIndex()
        {
            using (var db = new Context())
            {
                var model = new AddItemModel();

                UpdateModel(db, model);

                return View(model);
            }
        }

        // GET: RegisterItem
        public ActionResult Register(AddItemModel model)
        {
            using (var db = new Context())
            {
                UpdateModel(db, model);

                if (ModelState.IsValid)
                {
                    if (db.Items.Any(it => it.Name == model.Name))
                    {
                        ModelState.Clear();
                        ModelState.AddModelError("", "Item with this name already exists");
                    }
                    else
                    {
                        var color = model.Color.Single();
                        var type = model.Type.Single();

                        var item = new Item
                        {
                            AddedBy = User.Identity.GetUserName(),
                            Name = model.Name,
                            Color = db.ItemColors.First(ic => ic.Id == color),
                            Type = db.ItemTypes.First(it => it.Id == type),
                        };

                        db.Items.Add(item);
                        db.SaveChanges();

                        return RedirectToActionPermanent("Index", "Items");
                    }
                }

                return View("OldIndex", model);
            }
        }

        [HttpPost, JsonNetFilter]
        public JsonResult Register(RegisterItemModel item)
        {
            var claimsIdentity = (ClaimsIdentity) HttpContext.User.Identity;

            if (!claimsIdentity.ValidateRights(UserRights.AddEntities))
            {
                return Json(new
                {
                    Result = false,
                    Errors = new []
                    {
                        new ErrorModel
                        {
                            Code = ErrorCodes.NotEnoughRights,
                            Description = "You don't have rights to add entities",
                            AdditionalInfo = new
                            {
                                RequiredRights = UserRights.AddEntities,
                            }
                        },
                    },
                });
            }

            using (var db = new Context())
            {
                var errors = new List<ErrorModel>();
                var creeps = new List<Creep>();
                var bonuses = new List<ObjectStat>();
                var dbItem = db.Items.FirstOrDefault(it => it.Name == item.Name);
                var type = db.ItemTypes.FirstOrDefault(t => t.Id == item.ItemType);
                var color = db.ItemColors.FirstOrDefault(c => c.Id == item.ItemRarity);

                if (dbItem != null)
                {
                    errors.Add(new ErrorModel
                    {
                        Code = ErrorCodes.ItemAlreadyExists,
                        Description = "Item with this name already exists",
                    });
                }

                if (type == null)
                {
                    errors.Add(new ErrorModel
                    {
                        Code = ErrorCodes.ItemTypeNotFound,
                        Description = "Item type doesn't exist",
                    });
                }

                if (color == null)
                {
                    errors.Add(new ErrorModel
                    {
                        Code = ErrorCodes.ItemRarityNotFound,
                        Description = "Item rarity doesn't exist",
                    });
                }

                foreach (var creepId in item.Creeps)
                {
                    var creep = db.Creeps.FirstOrDefault(c => c.Id == creepId);

                    if (creep == null)
                    {
                        errors.Add(new ErrorModel
                        {
                            Code = ErrorCodes.CreepNotFound,
                            Description = "Creep doesn't exist",
                            AdditionalInfo = new
                            {
                                Id = creepId,
                            },
                        });
                    }
                    else
                    {
                        creeps.Add(creep);
                    }
                }

                foreach (var bonus in item.Bonuses)
                {
                    var stat = db.Stats.FirstOrDefault(s => s.Id == bonus.Stat);

                    if (stat == null)
                    {
                        errors.Add(new ErrorModel
                        {
                            Code = ErrorCodes.StatNotFound,
                            Description = "Stat doesn't exist",
                            AdditionalInfo = new
                            {
                                Id = bonus.Stat,
                            },
                        });
                    }
                    else
                    {
                        bonuses.Add(new ObjectStat
                        {
                            Stat = stat,
                            Value = bonus.Value,
                        });
                    }
                }

                if (errors.Any())
                {
                    return Json(new
                    {
                        Result = false,
                        Errors = errors,
                    });
                }

                dbItem = new Item
                {
                    Name = item.Name,
                    Type = type,
                    Color = color,
                    AddedBy = User.Identity.GetUserName(),
                };

                db.Items.Add(dbItem);

                foreach (var creep in creeps)
                {
                    db.CreepDrops.Add(new CreepDrop
                    {
                        Item = dbItem,
                        Creep = creep,
                    });
                }

                foreach (var bonus in bonuses)
                {
                    db.ObjectStats.Add(bonus);
                    db.ItemBonuses.Add(new ItemBonus
                    {
                        Bonus = bonus,
                        Item = dbItem,
                    });
                }

                db.SaveChanges();

                return Json(new
                {
                    Result = true,
                });
            }
        }

        private static void UpdateModel(Context db, AddItemModel model)
        {
            model.Types = db.ItemTypes.Select(it => new SelectListItem
            {
                Value = it.Id.ToString(),
                Text = it.Name,
            }).ToArray();
            model.Colors = db.ItemColors.Select(c => new SelectListItem
            {
                Value = c.Id.ToString(),
                Text = c.Name,
            }).ToArray();
        }
    }
}