﻿using System.Collections.Generic;

namespace Gaias.Dtos
{
    public class ItemDto
    {
        public long Id { get; set; }
        public string Name { get; set; } 
        public long TypeId { get; set; }
        public string Type { get; set; }
        public long RarityId { get; set; }
        public string Rarity { get; set; }
        public string Color { get; set; }
        public IEnumerable<CreepDto> Creeps { get; set; }
        public IEnumerable<BonusDto> Bonuses { get; set; }
    }
}