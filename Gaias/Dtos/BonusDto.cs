﻿namespace Gaias.Dtos
{
    public class BonusDto
    {
        public float Value { get; set; }
        public string Stat { get; set; }
    }
}