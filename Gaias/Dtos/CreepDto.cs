﻿namespace Gaias.Dtos
{
    public class CreepDto
    {
        public string Name { get; set; }
        public int Level { get; set; }
    }
}