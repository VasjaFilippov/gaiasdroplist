﻿using Gaias.Utilities;

namespace Gaias.Sorters
{
    public class Sorting
    {
        public string Column { get; set; }
        public bool SortAsc { get; set; }

        public Sorting()
        {
            
        }

        public Sorting(string column, bool sortAsc)
        {
            Column = column;
            SortAsc = sortAsc;
        }

        public Sorting(string column, string sortDir)
            : this(column, sortDir.ToSortAsc())
        {
            
        }
    }
}