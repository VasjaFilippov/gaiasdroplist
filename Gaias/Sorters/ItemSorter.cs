﻿using System.Linq;
using Gaias.Dtos;
using Gaias.Utilities;

namespace Gaias.Sorters
{
    public class ItemSorter : ISorter<ItemDto>
    {
        public IQueryable<ItemDto> Sort(IQueryable<ItemDto> query, Sorting sort, bool firstSort)
        {
            switch (sort?.Column?.ToLower())
            {
                case "id":
                    return query.Sort(sort.SortAsc, i => i.Id, firstSort);
                case "name":
                    return query.Sort(sort.SortAsc, i => i.Name, firstSort);
                case "rarity":
                    return query.Sort(sort.SortAsc, i => i.Rarity, firstSort);
                case "type":
                    return query.Sort(sort.SortAsc, i => i.Type, firstSort);
                case "color":
                    return query.Sort(sort.SortAsc, i => i.Color, firstSort);
                default:
                    return query;
            }
        }

        public IQueryable<ItemDto> Sort(IQueryable<ItemDto> query, Sorting[] sorts, bool firstSort = true)
        {
            if (sorts.Length == 0)
                return query;

            query = Sort(query, sorts[0], firstSort);

            if (sorts.Length <= 1)
                return query;

            for (var i = 1; i < sorts.Length; i++)
            {
                query = Sort(query, sorts[i], false);
            }

            return query;
        }
    }
}