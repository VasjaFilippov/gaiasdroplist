﻿using System.Linq;

namespace Gaias.Sorters
{
    public interface ISorter<TValue>
    {
        IQueryable<TValue> Sort(IQueryable<TValue> query, Sorting sort, bool firstSort);
        IQueryable<TValue> Sort(IQueryable<TValue> query, Sorting[] sorts, bool firstSort = true);
    }
}