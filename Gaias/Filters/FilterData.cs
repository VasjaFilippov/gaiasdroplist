﻿namespace Gaias.Filters
{
    public class FilterData
    {
        public string Column { get; set; }
        public string Filter { get; set; }
    }
}