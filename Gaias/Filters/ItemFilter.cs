﻿using System.Linq;
using Gaias.Dtos;

namespace Gaias.Filters
{
    public class ItemFilter : IFilter<ItemDto>
    {
        public IQueryable<ItemDto> Filter(IQueryable<ItemDto> query, FilterData filter)
        {
            switch (filter.Column)
            {
                case "Name":
                    return query.Where(i => i.Name.Contains(filter.Filter));
                case "Rarity":
                    return query.Where(i => i.RarityId.ToString() == filter.Filter);
                case "Type":
                    return query.Where(i => i.TypeId.ToString() == filter.Filter);
                default:
                    return query;
            }
        }
    }
}