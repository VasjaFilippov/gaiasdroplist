﻿using System.Linq;

namespace Gaias.Filters
{
    public interface IFilter<TValue>
    {
        IQueryable<TValue> Filter(IQueryable<TValue> query, FilterData filter);
    }
}