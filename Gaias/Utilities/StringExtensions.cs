﻿namespace Gaias.Utilities
{
    public static class StringExtensions
    {
        public static bool ToSortAsc(this string str)
        {
            var lstr = str.ToLower();
            return lstr == "ascending"
                || lstr == "asc";
        }
    }
}