﻿using System.Linq;
using System.Security.Claims;
using Gaias.Enums;
using Gaias.Models;

namespace Gaias.Utilities
{
    public static class IdentityExtensions
    {
        public static bool ValidateRights(this ClaimsIdentity identity, UserRights rightsToValidate)
        {
            Claim rightsClaim;
            if (identity.Claims.All(c => c.Type != "Rights"))
            {
                rightsClaim = new Claim("Rights", ((int) ApplicationUser.DefaultRights).ToString());
                identity.AddClaim(rightsClaim);
            }
            else
            {
                rightsClaim = identity.FindFirst("Rights");
            }

            var rights = (UserRights) int.Parse(rightsClaim.Value);

            return rights.HasFlag(rightsToValidate);
        }
    }
}