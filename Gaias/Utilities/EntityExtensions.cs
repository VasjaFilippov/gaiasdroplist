﻿using System.Linq;
using System.Web.Mvc;
using Gaias.Db.Entities;

namespace Gaias.Utilities
{
    public static class EntityExtensions
    {
        public static IQueryable<SelectListItem> ToSelectListItems(this IQueryable<GaiasObject> query)
        {
            return query
                .Select(obj => new SelectListItem
                {
                    Value = obj.Id.ToString(),
                    Text = obj.Name,
                });
        }

        public static SelectListItem[] ToSelectListItemsArray(this IQueryable<GaiasObject> query)
        {
            return query
                .ToSelectListItems()
                .ToArray();
        }
    }
}