﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Gaias.Filters;
using Gaias.Models;
using Gaias.Sorters;

namespace Gaias.Utilities
{
    public static class QueriableExtensions
    {
        public static IOrderedQueryable<TValue> Sort<TValue, TProperty>(this IQueryable<TValue> querry, bool sortAsc, Expression<Func<TValue, TProperty>> property, bool firstSort)
        {
            if (firstSort)
                return sortAsc
                    ? querry.OrderBy(property)
                    : querry.OrderByDescending(property);

            var ordered = (IOrderedQueryable<TValue>) querry;

            return sortAsc
                ? ordered.ThenBy(property)
                : ordered.ThenByDescending(property);
        }
        
        public static IQueryable<TValue> Paging<TValue>(this IQueryable<TValue> querry, int pageSize, int page)
        {
            return querry
                .Skip(pageSize*page)
                .Take(pageSize);
        }

        public static IQueryable<TValue> Sort<TValue>(this IQueryable<TValue> query, ISorter<TValue> sorter, Sorting sort, bool firstSort)
        {
            return sorter
                .Sort(query, sort, firstSort);
        }

        public static IQueryable<TValue> Sort<TValue>(this IQueryable<TValue> query, ISorter<TValue> sorter, Sorting[] sorts, bool firstSort = true)
        {
            return sorter
                .Sort(query, sorts, firstSort);
        }

        public static IQueryable<TValue> Filter<TValue>(this IQueryable<TValue> query, IFilter<TValue> filter, FilterData filterData)
        {
            return filter
                .Filter(query, filterData);
        }

        public static IQueryable<TValue> Filter<TValue>(this IQueryable<TValue> query, IFilter<TValue> filter, IEnumerable<FilterData> filterDatas)
        {
            return filterDatas
                .Aggregate(query, filter.Filter);
        }

        public static TOut[] GenerateTable<TIn, TDto, TOut>(
            this IQueryable<TIn> rawData,
            TableModel parameters,
            Expression<Func<TIn, TDto>> inToDto,
            Func<TDto, TOut> dtoToModel,
            IFilter<TDto> filter,
            ISorter<TDto> sorter,
            IEnumerable<FilterData> defaultFilters,
            Sorting[] defaultSorts,
            out int totalPages,
            out int pageNumber)
        {
            var unpagedData = rawData
                .Select(inToDto)
                .Filter(filter, parameters.Filters ?? defaultFilters);

            var total = unpagedData.Count();
            var paging = new Paging(parameters.Page, parameters.PageSize.Limit(1, total));

            pageNumber = paging.SafeNumber(total);
            totalPages = total.CeilDiv(paging.Count);

            var data = unpagedData
                .Sort(sorter, parameters.Sorts ?? defaultSorts)
                .Paging(paging.Count, pageNumber - 1)
                .ToArray()
                .Select(dtoToModel)
                .ToArray();

            return data;
        }
    }
}