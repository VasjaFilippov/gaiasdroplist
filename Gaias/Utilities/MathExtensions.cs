﻿using System;

namespace Gaias.Utilities
{
    public static class MathExtensions
    {
        public static int Limit(this int value, int min, int max)
        {
            if (value <= min)
                return min;

            if (value >= max)
                return max;

            return value;
        }

        public static int CeilDiv(this int left, int right)
        {
            return (int) Math.Ceiling((double) left/right);
        }
    }
}