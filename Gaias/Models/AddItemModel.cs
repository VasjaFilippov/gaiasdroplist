﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Gaias.Models
{
    public class AddItemModel
    {
        [Required]
        [Display(Name = "Item name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Item color")]
        public IEnumerable<long> Color { get; set; }

        [Required]
        [Display(Name = "Item type")]
        public IEnumerable<long> Type { get; set; }

        public SelectListItem[] Colors { get; set; }
        public SelectListItem[] Types { get; set; }
    }
}