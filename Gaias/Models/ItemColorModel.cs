﻿namespace Gaias.Models
{
    public class ItemColorModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
    }
}