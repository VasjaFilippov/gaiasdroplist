﻿namespace Gaias.Models
{
    public class RegisterItemModel
    {
        public string Name { get; set; } 
        public long ItemType { get; set; }
        public long ItemRarity { get; set; }
        public RegisterBonusModel[] Bonuses { get; set; } 
        public long[] Creeps { get; set; }
    }
}