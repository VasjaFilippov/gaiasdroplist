﻿namespace Gaias.Models
{
    public class ItemTypeModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}