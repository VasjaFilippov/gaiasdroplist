﻿using Gaias.Filters;
using Gaias.Sorters;

namespace Gaias.Models
{
    public class TableModel
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public FilterData[] Filters { get; set; }
        public Sorting[] Sorts { get; set; }
    }
}