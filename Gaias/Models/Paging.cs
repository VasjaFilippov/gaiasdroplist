﻿using Gaias.Utilities;

namespace Gaias.Models
{
    public struct Paging
    {
        public int Number { get; set; } 
        public int Count { get; set; }

        public Paging(int number, int count)
        {
            Number = number;
            Count = count;
        }

        public int SafeNumber(int total)
        {
            return Number.Limit(1, total.CeilDiv(Count));
        }
    }
}