﻿using Gaias.Enums;

namespace Gaias.Models
{
    public class ErrorModel
    {
        public ErrorCodes Code { get; set; }
        public string Description { get; set; }
        public object AdditionalInfo { get; set; }
    }
}