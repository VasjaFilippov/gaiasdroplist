﻿using System.Collections.Generic;
using Gaias.Dtos;

namespace Gaias.Models
{
    public class ItemModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public ItemTypeModel ItemType { get; set; }
        public ItemColorModel ItemRarity { get; set; }
        public IEnumerable<CreepDto> Creeps { get; set; }
        public IEnumerable<BonusDto> ItemBonuses { get; set; }

        public ItemModel(ItemDto dto)
        {
            Id = dto.Id;
            Name = dto.Name;
            ItemType = new ItemTypeModel
            {
                Id = dto.TypeId,
                Name = dto.Type,
            };
            ItemRarity = new ItemColorModel
            {
                Id = dto.RarityId,
                Name = dto.Rarity,
                Color = dto.Color,
            };
            ItemBonuses = dto.Bonuses;
            Creeps = dto.Creeps;
        }
    }
}