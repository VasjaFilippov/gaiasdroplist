﻿import {GaiasEvent, IPublicEvent} from "../Utility/Event"
import {ElementClasses, ApplyClass} from "../Utility/StyleUtilities"

export interface IButtonStyle {
    ButtonStyle?: ElementClasses;
}

export class Button {
    public get Click(): IPublicEvent<Button> {
        return this._click;
    }

    public get Style(): IButtonStyle {
        return this._style;
    }

    public set Style(value: IButtonStyle) {
        this.ApplyStyle(value);
        this._style = value;
    }

    public constructor() {
        this._button = $("<button/>")
            .click(() => this._click.Invoke(this));
    }

    public SetStyle(value: IButtonStyle): Button {
        this.Style = value;
        return this;
    }

    public AppendTo(container: JQuery): Button {
        this._button.appendTo(container);
        return this;
    }

    public InsertAfter(container: JQuery): Button {
        this._button.insertAfter(container);
        return this;
    }

    public SetButtonText(text: JQuery | any[] | Element | DocumentFragment | Text | string): Button {
        this._button.empty().append(text);
        return this;
    }

    private ApplyStyle(style: IButtonStyle) {
        ApplyClass(this._style, style, s => s.ButtonStyle, this._button);
    }

    private _click = new GaiasEvent<Button>();
    private _button: JQuery;
    private _style: IButtonStyle = {};

    public static ParseButton(element: JQuery): Button {
        const button = new Button();
        const jqElement = $(element);

        button.Style = { ButtonStyle: (jqElement.attr("class") || "").split(" ") };
        button.SetButtonText(jqElement.html());
        button.InsertAfter(jqElement);
        jqElement.remove();

        return button;
    }
}