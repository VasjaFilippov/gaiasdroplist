define(["require", "exports", "../Utility/Event", "../Utility/StyleUtilities"], function (require, exports, Event_1, StyleUtilities_1) {
    "use strict";
    var Button = (function () {
        function Button() {
            var _this = this;
            this._click = new Event_1.GaiasEvent();
            this._style = {};
            this._button = $("<button/>")
                .click(function () { return _this._click.Invoke(_this); });
        }
        Object.defineProperty(Button.prototype, "Click", {
            get: function () {
                return this._click;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Button.prototype, "Style", {
            get: function () {
                return this._style;
            },
            set: function (value) {
                this.ApplyStyle(value);
                this._style = value;
            },
            enumerable: true,
            configurable: true
        });
        Button.prototype.SetStyle = function (value) {
            this.Style = value;
            return this;
        };
        Button.prototype.AppendTo = function (container) {
            this._button.appendTo(container);
            return this;
        };
        Button.prototype.InsertAfter = function (container) {
            this._button.insertAfter(container);
            return this;
        };
        Button.prototype.SetButtonText = function (text) {
            this._button.empty().append(text);
            return this;
        };
        Button.prototype.ApplyStyle = function (style) {
            StyleUtilities_1.ApplyClass(this._style, style, function (s) { return s.ButtonStyle; }, this._button);
        };
        Button.ParseButton = function (element) {
            var button = new Button();
            var jqElement = $(element);
            button.Style = { ButtonStyle: (jqElement.attr("class") || "").split(" ") };
            button.SetButtonText(jqElement.html());
            button.InsertAfter(jqElement);
            jqElement.remove();
            return button;
        };
        return Button;
    }());
    exports.Button = Button;
});
//# sourceMappingURL=Button.js.map