define(["require", "exports", "./Table"], function (require, exports, Table_1) {
    "use strict";
    var TableValueFormaters;
    (function (TableValueFormaters) {
        var SorterPlainTextFormater = (function () {
            function SorterPlainTextFormater(column, defaultSort) {
                if (defaultSort === void 0) { defaultSort = 0 /* None */; }
                this._column = column;
                this._state = defaultSort;
            }
            SorterPlainTextFormater.prototype.CreateCell = function (table, row) {
                var _this = this;
                this._arrow = $("<span/>");
                this._text = $("<span/>")
                    .css("text-decoration", "underline");
                var root = $("<div/>")
                    .append(this._text, this._arrow)
                    .click(function () { return ApplySort(table, _this._column, NextSort(_this._state)); });
                table.SortsChanged.AddHandler(function () {
                    var sorting = table.GetSort(_this._column);
                    var newState;
                    if (sorting) {
                        if (sorting.SortAsc) {
                            newState = 1 /* Ascending */;
                        }
                        else {
                            newState = 2 /* Descending */;
                        }
                    }
                    else {
                        newState = 0 /* None */;
                    }
                    if (newState !== _this._state) {
                        _this._state = newState;
                        ApplyArrow(_this._arrow, _this._state);
                    }
                }, this);
                if (this._state !== 0 /* None */) {
                    ApplySort(table, this._column, this._state);
                    ApplyArrow(this._arrow, this._state);
                }
                return root;
            };
            SorterPlainTextFormater.prototype.UpdateValue = function (value, row) {
                this._text.text(value);
            };
            return SorterPlainTextFormater;
        }());
        TableValueFormaters.SorterPlainTextFormater = SorterPlainTextFormater;
        function ApplySort(table, column, state) {
            switch (state) {
                case 0 /* None */:
                    table.RemoveSort(column);
                    break;
                case 1 /* Ascending */:
                    table.Sorts = [new Table_1.TableSorting(column, true)];
                    break;
                case 2 /* Descending */:
                    table.Sorts = [new Table_1.TableSorting(column, false)];
                    break;
            }
        }
        function ApplyArrow(arrow, state) {
            switch (state) {
                case 0 /* None */:
                    arrow.text("");
                    break;
                case 1 /* Ascending */:
                    arrow.text("↓");
                    break;
                case 2 /* Descending */:
                    arrow.text("↑");
                    break;
            }
        }
        function NextSort(sort) {
            switch (sort) {
                case 1 /* Ascending */:
                    return 2 /* Descending */;
                case 0 /* None */:
                case 2 /* Descending */:
                default:
                    return 1 /* Ascending */;
            }
        }
    })(TableValueFormaters = exports.TableValueFormaters || (exports.TableValueFormaters = {}));
});
//# sourceMappingURL=TableValueFormaters.js.map