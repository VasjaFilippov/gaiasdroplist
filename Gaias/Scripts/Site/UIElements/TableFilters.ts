﻿import {Button} from "./Button"
import {Ajax} from "../Utility/Ajax"
import {Form, FormField} from "./Form/Form"
import {TableBase, TableFilter} from "./Table"
import {ElementClasses, ApplyClass} from "../Utility/StyleUtilities"
import {ChangeTag} from "../Utility/JqUtils"

export class TableFilterField {
    public get Column(): string {
        return this._column;
    }

    public get Field(): FormField {
        return this._field;
    }

    public constructor(column: string, field: FormField) {
        this._column = column;
        this._field = field;
    }

    private _column: string;
    private _field: FormField;
}

export interface ITableFilterApplierStyle {
    ButtonStyle?: ElementClasses;
}

$(document).ready(() => $("gps-filter-form").each((id, formElem) => {
    const jqForm = $(formElem);
    const table = TableBase.TablesTable[jqForm.attr("table")];
    const filters = new Array<TableFilterField>();
    const appliers = new Array<Button>();
    
    Form.ParseForm(jqForm, (field, elem) => {
        const jqElem = $(elem);
        const column = jqElem.attr("column");

        if (field.Caption.length === 0) {
            field.SetCaption(column);
        }

        filters.push(new TableFilterField(column, field));
    }, button => appliers.push(button));

    appliers.forEach(applier =>
        applier.Click.AddHandler(() => {
            const tableFilters = new Array<TableFilter>();

            filters.forEach(filter => {
                const value = filter.Field.GetValue();

                if (value.length > 0 && value != -1) {
                    tableFilters.push(new TableFilter(filter.Column, value));
                }
            });

            table.Filters = tableFilters;
        }, this));

    jqForm.attr("table", undefined);
    ChangeTag(formElem, "div");
}))