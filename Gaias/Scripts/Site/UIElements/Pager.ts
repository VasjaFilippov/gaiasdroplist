﻿import {ElementClasses} from "../Utility/StyleUtilities"
import {TableBase, IElementsTable} from "./Table"

export interface IPagerStyle {
    PagerClasses: ElementClasses;
    ElementClasses: ElementClasses;
    BorderElementClasses: ElementClasses;
    InactiveElementClasses: ElementClasses;
}

export interface IPagerParams {
    Name: string;
    PageLimit: number;
    Table: TableBase | string;
    Container: JQuery;
}

export class Pager {
    public get Name(): string {
        return this._name;
    }

    public get PagesLimit(): number {
        return this._pagesLimit;
    }

    public set PagesLimit(value: number) {
        if (this.PagesLimit === value)
            return;

        this._pagesLimit = value;
        this.Update();
    }

    public get Page(): number {
        return this._table.Page;
    }

    public set Page(value: number) {
        this._table.Page = value;
    }

    public get Pages(): number {
        return this._table.Pages;
    }

    public constructor(params: IPagerParams) {
        Pager.PagersTable[params.Name] = this;
        this._name = params.Name;
        this._container = params.Container;

        if (typeof params.Table === "string") {
            this._table = TableBase.TablesTable[params.Table as string];
        } else {
            this._table = params.Table as TableBase;
        }

        this._pager = $("<ul>")
            .addClass("pager");

        this._container.append(this._pager);
        this.PagesLimit = params.PageLimit;

        this._table.AfterUpdate.AddHandler(() => Pager.PagersTable[this._name].Update(), this);
    }

    public GoToNext(): void {
        if (this.Page < this.Pages) {
            this.Page++;
        }
    }

    public GoToPrev(): void {
        if (this.Page > 1) {
            this.Page--;
        }
    }

    public GoToLast(): void {
        if (this.Page < this.Pages) {
            this.Page = this.Pages;
        }
    }

    public GoToFirst(): void {
        if (this.Page > 1) {
            this.Page = 1;
        }
    }

    public Update() {
        this._pager.empty();

        let min = this.Page - this.PagesLimit;
        let max = this.Page + this.PagesLimit;

        const firstA = $("<a/>")
            .text("First");
        const first = $("<li/>")
            .addClass("previous")
            .append(firstA);

        const prevA = $("<a/>")
            .text("Previous");
        const prev = $("<li/>")
            .addClass("previous")
            .append(prevA);
        
        const nextA = $("<a/>")
            .text("Next");
        const next = $("<li/>")
            .addClass("next")
            .append(nextA);

        const lastA = $("<a/>")
            .text("Last");
        const last = $("<li/>")
            .addClass("next")
            .append(lastA);

        this._pager
            .append(first)
            .append(prev);

        if (min < 1) {
            min = 1;
            first.addClass("disabled");
        } else {
            firstA
                .attr("href", "javascript:void(0)")
                .click(() => Pager.PagersTable[this._name].Page = 1);
        }

        if (this.Page < 2) {
            prev.addClass("disabled");
        } else {
            prevA
                .attr("href", "javascript:void(0)")
                .click(() => Pager.PagersTable[this._name].Page = this.Page - 1);
        }

        if (max > this.Pages) {
            max = this.Pages;
            last.addClass("disabled");
        } else {
            lastA
                .attr("href", "javascript:void(0)")
                .click(() => Pager.PagersTable[this._name].Page = this.Pages);
        }

        if (this.Page + 1 > this.Pages) {
            next.addClass("disabled");
        } else {
            nextA
                .attr("href", "javascript:void(0)")
                .click(() => Pager.PagersTable[this._name].Page = this.Page + 1);
        }

        for (let i = min; i <= max; i++) {
            const elemA = $("<a/>")
                .text(i);
            const elem = $("<li/>")
                .append(elemA);

            if (i === this.Page) {
                elem.addClass("disabled");
            } else {
                elemA
                    .attr("href", "javascript:void(0)")
                    .click(() => Pager.PagersTable[this._name].Page = i);
            }

            this._pager.append(elem);
        }

        this._pager
            .append(last)
            .append(next);
    }
    
    private _name: string;
    private _pagesLimit: number;
    private _container: JQuery;
    private _pager: JQuery;
    private _table: TableBase;
    private _style: IPagerStyle;

    public static get PagersTable(): IElementsTable<Pager> {
        (window as any).Pagers = (window as any).Pagers || {};
        return (window as any).Pagers;
    }
}

$(document).ready(() => $("gps-pager").each((id, pagerElem) => {
    const jqPager = $(pagerElem);
    const container = $("<div/>");
    const pager = new Pager({
        Name: jqPager.attr("name"),
        Container: container,
        PageLimit: +jqPager.attr("page-limit") || 5,
        Table: jqPager.attr("table")
    });

    container.insertAfter(jqPager);
    jqPager.remove();
}));