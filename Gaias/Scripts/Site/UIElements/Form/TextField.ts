﻿import {Form, FormField, IFormFieldStyle} from "./Form"

export class FormTextField extends FormField{
    constructor() {
        const field = $("<input/>")
            .attr("type", "text");

        super(field);
    }

    public SetValue(value: string | number | string[]): FormTextField {
        this.Field.val(value);
        return this;
    }

    public SetStyle(style: IFormFieldStyle): FormTextField {
        return super.SetStyle(style) as FormTextField;
    }

    public SetCaption(value: JQuery | any[] | Element | DocumentFragment | Text | string): FormTextField {
        return super.SetCaption(value) as FormTextField;
    }

    public SetValuePreparer(preparer: (value: any) => any): FormTextField {
        return super.SetValuePreparer(preparer) as FormTextField;
    }

    public AppendTo(container: JQuery): FormTextField {
        return super.AppendTo(container) as FormTextField;
    }

    public InsertAfter(elem: JQuery): FormTextField {
        return super.InsertAfter(elem) as FormTextField;
    }

    protected GetValueOverride(): any {
        return this.Field.val();
    }
}

Form.RegisterFieldType("text", () => new FormTextField());