﻿import {Ajax} from "../../Utility/Ajax"
import {Form, FormField, IFormFieldStyle} from "./Form"

export class DropdownFormField extends FormField {
    public constructor() {
        const field = $("<select/>")
            .attr("type", "text");

        super(field);
    }

    public SetValue(value: string | number | string[]): DropdownFormField {
        this.Field.val(value);
        return this;
    }

    public AddOption(value: string | string[] | number, content: JQuery | any[] | Element | DocumentFragment | Text | string): DropdownFormField {
        this.Field.append($("<option/>")
            .val(value)
            .append(content));
        return this;
    }

    public AddOptions(options: any[], valueGetter: (option: any) => string | string[] | number, optionGetter: (option: any) => JQuery | any[] | Element | DocumentFragment | Text | string): DropdownFormField {
        options.forEach(option => this.Field
            .val(valueGetter(option))
            .append($("<option/>").append(option)));
        return this;
    }

    public AddOptionsFromUrl(url: string, optionsGetter: (data: any) => any[], valueGetter: (option: any) => string | string[] | number, optionGetter: (option: any) => JQuery | any[] | Element | DocumentFragment | Text | string): DropdownFormField {
        const request = new Ajax(url, (data) => {
            const options = optionsGetter(data);
            options.forEach(option => this.Field.append($("<option/>")
                .val(valueGetter(option))
                .append(optionGetter(option))));
        });

        request.Send();
        return this;
    }

    public AppendTo(container: JQuery): DropdownFormField {
        return super.AppendTo(container) as DropdownFormField;
    }

    public InsertAfter(elem: JQuery): DropdownFormField {
        return super.InsertAfter(elem) as DropdownFormField;
    }

    public SetStyle(style: IFormFieldStyle): DropdownFormField {
        return super.SetStyle(style) as DropdownFormField;
    }

    public SetCaption(value: JQuery | any[] | Element | DocumentFragment | Text | string): DropdownFormField {
        return super.SetCaption(value) as DropdownFormField;
    }

    public SetValuePreparer(preparer: (value: any) => any): DropdownFormField {
        return super.SetValuePreparer(preparer) as DropdownFormField;
    }

    protected GetValueOverride(): any {
        return this.Field.val();
    }
}

Form.RegisterFieldType("dropdown", element => {
    const field = new DropdownFormField();
    const jqField = $(element);
    const addAny = !!jqField.attr("add-any");

    if (addAny) {
        field.AddOption(-1, "Any");
        field.SetValuePreparer(value => value == -1 ? "" : value);
    }

    jqField.contents().filter("option").each((optionId, option) => {
        const jqOption = $(option);
        field.AddOption(jqOption.attr("value"), jqOption.text());
    });

    jqField.contents().filter("url").each((urlId, url) => {
        const jqUrl = $(url);
        field.AddOptionsFromUrl(
            jqUrl.attr("url"),
            Function("data", `return ${jqUrl.attr("data")}`) as (data: any) => any[],
            Function("row", `return ${jqUrl.attr("value")}`) as (row: any) => string | string[] | number,
            Function("row", `return ${jqUrl.html()}`) as (row: any) => JQuery | Element | any[] | DocumentFragment | Text | string);
    });

    return field;
});