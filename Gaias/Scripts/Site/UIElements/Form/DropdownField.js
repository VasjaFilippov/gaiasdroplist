var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "../../Utility/Ajax", "./Form"], function (require, exports, Ajax_1, Form_1) {
    "use strict";
    var DropdownFormField = (function (_super) {
        __extends(DropdownFormField, _super);
        function DropdownFormField() {
            var field = $("<select/>")
                .attr("type", "text");
            _super.call(this, field);
        }
        DropdownFormField.prototype.SetValue = function (value) {
            this.Field.val(value);
            return this;
        };
        DropdownFormField.prototype.AddOption = function (value, content) {
            this.Field.append($("<option/>")
                .val(value)
                .append(content));
            return this;
        };
        DropdownFormField.prototype.AddOptions = function (options, valueGetter, optionGetter) {
            var _this = this;
            options.forEach(function (option) { return _this.Field
                .val(valueGetter(option))
                .append($("<option/>").append(option)); });
            return this;
        };
        DropdownFormField.prototype.AddOptionsFromUrl = function (url, optionsGetter, valueGetter, optionGetter) {
            var _this = this;
            var request = new Ajax_1.Ajax(url, function (data) {
                var options = optionsGetter(data);
                options.forEach(function (option) { return _this.Field.append($("<option/>")
                    .val(valueGetter(option))
                    .append(optionGetter(option))); });
            });
            request.Send();
            return this;
        };
        DropdownFormField.prototype.AppendTo = function (container) {
            return _super.prototype.AppendTo.call(this, container);
        };
        DropdownFormField.prototype.InsertAfter = function (elem) {
            return _super.prototype.InsertAfter.call(this, elem);
        };
        DropdownFormField.prototype.SetStyle = function (style) {
            return _super.prototype.SetStyle.call(this, style);
        };
        DropdownFormField.prototype.SetCaption = function (value) {
            return _super.prototype.SetCaption.call(this, value);
        };
        DropdownFormField.prototype.SetValuePreparer = function (preparer) {
            return _super.prototype.SetValuePreparer.call(this, preparer);
        };
        DropdownFormField.prototype.GetValueOverride = function () {
            return this.Field.val();
        };
        return DropdownFormField;
    }(Form_1.FormField));
    exports.DropdownFormField = DropdownFormField;
    Form_1.Form.RegisterFieldType("dropdown", function (element) {
        var field = new DropdownFormField();
        var jqField = $(element);
        var addAny = !!jqField.attr("add-any");
        if (addAny) {
            field.AddOption(-1, "Any");
            field.SetValuePreparer(function (value) { return value == -1 ? "" : value; });
        }
        jqField.contents().filter("option").each(function (optionId, option) {
            var jqOption = $(option);
            field.AddOption(jqOption.attr("value"), jqOption.text());
        });
        jqField.contents().filter("url").each(function (urlId, url) {
            var jqUrl = $(url);
            field.AddOptionsFromUrl(jqUrl.attr("url"), Function("data", "return " + jqUrl.attr("data")), Function("row", "return " + jqUrl.attr("value")), Function("row", "return " + jqUrl.html()));
        });
        return field;
    });
});
//# sourceMappingURL=DropdownField.js.map