var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var FormTextField = (function (_super) {
    __extends(FormTextField, _super);
    function FormTextField() {
        _super.call(this);
        this._style = {};
        this._label = $("<label/>");
        this._field = $("<input/>")
            .attr("type", "text");
        this._element = $("<div/>")
            .append(this._label, this._field);
    }
    Object.defineProperty(FormTextField.prototype, "Style", {
        get: function () {
            return this._style;
        },
        set: function (style) {
            style = style || {};
            this.ApplyStyle(style);
            this._style = style;
        },
        enumerable: true,
        configurable: true
    });
    FormTextField.prototype.GetRoot = function () {
        return this._element;
    };
    FormTextField.prototype.SetValue = function (value) {
        this._field.val(value);
        return this;
    };
    FormTextField.prototype.SetCaption = function (value) {
        this._label.append(value);
        return this;
    };
    FormTextField.prototype.SetStyle = function (style) {
        this.Style = style;
        return this;
    };
    FormTextField.prototype.GetValueOverride = function () {
        return this._field.val();
    };
    FormTextField.prototype.ApplyStyle = function (style) {
        ApplyClass(this._style, style, function (s) { return s.RootStyle; }, this._element);
        ApplyClass(this._style, style, function (s) { return s.LabelStyle; }, this._label);
        ApplyClass(this._style, style, function (s) { return s.FieldStyle; }, this._field);
    };
    return FormTextField;
}(FormFieldBase));
//# sourceMappingURL=FormTextField.js.map