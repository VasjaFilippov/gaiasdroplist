﻿import {Button} from "../Button"
import {ElementClasses, ApplyClass} from "../../Utility/StyleUtilities"

export interface IFormFieldStyle {
    RootStyle?: ElementClasses;
    LabelStyle?: ElementClasses;
    FieldStyle?: ElementClasses;
}

export abstract class Form {
    public static ParseForm(container: JQuery, fieldHandler?: (field: FormField, elem: Element) => void, applierHandler?: (applier: any, elem: Element) => void): void {
        container
            .contents()
            .each((elemId, elem) => Form.CheckFormElement(elemId, elem, fieldHandler, applierHandler));
    }

    public static RegisterFieldType(fieldType: string, parser: (element: Element) => FormField): void {
        this.Parsers[fieldType] = parser;
    }

    public static ParseField(fieldElem: Element): FormField {
        const jqField = $(fieldElem);
        const fieldType = jqField.attr("type");

        if (!fieldType)
            throw new Error("Field type is undefined");

        const parser = this.Parsers[fieldType];

        if (!parser)
            throw new Error("Field type is unknown");

        const field = parser(fieldElem);

        jqField.contents().each((elemId, elem) => {
            const jqElem = $(elem);

            field.SetCaption(jqElem.attr("caption"));
            field.SetValue(jqElem.attr("value"));

            switch ((elem.tagName || "").toLowerCase()) {
                case "style":
                    field.Style = {
                        RootStyle: jqElem.attr("root").split(" "),
                        LabelStyle: jqElem.attr("label").split(" "),
                        FieldStyle: jqElem.attr("field").split(" ")
                    };
                    break;
            }
        });

        return field;
    }

    private static Parsers: { [fieldType: string]: (element: Element) => FormField; } = {};

    private static CheckFormElement(elemId: number, elem: Element, fieldHandler?: (field: FormField, elem: Element) => void, buttonHandler?: (applier: Button, elem: Element) => void): void {
        const jqElem = $(elem);

        switch ((elem.tagName || "").toLowerCase()) {
            case "gps-field":
                const field = Form.ParseField(elem)
                    .InsertAfter(jqElem);

                if (fieldHandler) {
                    fieldHandler(field, elem);
                }

                jqElem.remove();
                break;
            case "gps-button":
                if (!jqElem.attr("link-to-form"))
                    return;

                const button = Button.ParseButton(jqElem);

                if (buttonHandler) {
                    buttonHandler(button, elem);
                }
                break;
            default:
                $(elem)
                    .contents()
                    .each((elemId, elem) => Form.CheckFormElement(elemId, elem, fieldHandler, buttonHandler));
                break;
        }
    }
}

export abstract class FormField {
    public get Style(): IFormFieldStyle {
        return this._style;
    }

    public set Style(style: IFormFieldStyle) {
        style = style || {}
        this.ApplyStyle(style);
        this._style = style;
    }

    public get Caption(): string {
        return this._label.text();
    }

    public constructor(field: JQuery) {
        this._label = $("<label/>");

        this._field = field;

        this._element = $("<div/>")
            .append(this._label, this._field);
    }
    
    public abstract SetValue(value: any): FormField;

    public SetStyle(style: IFormFieldStyle): FormField {
        this.Style = style;
        return this;
    }

    public SetCaption(value: JQuery | any[] | Element | DocumentFragment | Text | string): FormField {
        this._label.append(value);
        return this;
    }

    public GetRoot(): JQuery {
        return this._element;
    }

    public GetValue(): any {
        const value = this.GetValueOverride();
        return this._valuePreparer(value);
    }

    public SetValuePreparer(preparer: (value: any) => any): FormField {
        this._valuePreparer = preparer || ((value: any) => value);
        return this;
    }

    public AppendTo(container: JQuery): FormField {
        container.append(this.GetRoot());
        return this;
    }

    public InsertAfter(elem: JQuery): FormField {
        this.GetRoot().insertAfter(elem);
        return this;
    }

    protected abstract GetValueOverride(): any;

    protected get Field() {
        return this._field;
    }

    private _valuePreparer: (value: any) => any = (value) => value;
    private _style: IFormFieldStyle = {};
    private _element: JQuery;
    private _label: JQuery;
    private _field: JQuery;

    private ApplyStyle(style: IFormFieldStyle): void {
        ApplyClass(this._style, style, s => s.RootStyle, this._element);
        ApplyClass(this._style, style, s => s.LabelStyle, this._label);
        ApplyClass(this._style, style, s => s.FieldStyle, this._field);
    }
}