var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./Form"], function (require, exports, Form_1) {
    "use strict";
    var FormTextField = (function (_super) {
        __extends(FormTextField, _super);
        function FormTextField() {
            var field = $("<input/>")
                .attr("type", "text");
            _super.call(this, field);
        }
        FormTextField.prototype.SetValue = function (value) {
            this.Field.val(value);
            return this;
        };
        FormTextField.prototype.SetStyle = function (style) {
            return _super.prototype.SetStyle.call(this, style);
        };
        FormTextField.prototype.SetCaption = function (value) {
            return _super.prototype.SetCaption.call(this, value);
        };
        FormTextField.prototype.SetValuePreparer = function (preparer) {
            return _super.prototype.SetValuePreparer.call(this, preparer);
        };
        FormTextField.prototype.AppendTo = function (container) {
            return _super.prototype.AppendTo.call(this, container);
        };
        FormTextField.prototype.InsertAfter = function (elem) {
            return _super.prototype.InsertAfter.call(this, elem);
        };
        FormTextField.prototype.GetValueOverride = function () {
            return this.Field.val();
        };
        return FormTextField;
    }(Form_1.FormField));
    exports.FormTextField = FormTextField;
    Form_1.Form.RegisterFieldType("text", function () { return new FormTextField(); });
});
//# sourceMappingURL=TextField.js.map