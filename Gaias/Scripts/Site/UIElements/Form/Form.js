define(["require", "exports", "../Button", "../../Utility/StyleUtilities"], function (require, exports, Button_1, StyleUtilities_1) {
    "use strict";
    var Form = (function () {
        function Form() {
        }
        Form.ParseForm = function (container, fieldHandler, applierHandler) {
            container
                .contents()
                .each(function (elemId, elem) { return Form.CheckFormElement(elemId, elem, fieldHandler, applierHandler); });
        };
        Form.RegisterFieldType = function (fieldType, parser) {
            this.Parsers[fieldType] = parser;
        };
        Form.ParseField = function (fieldElem) {
            var jqField = $(fieldElem);
            var fieldType = jqField.attr("type");
            if (!fieldType)
                throw new Error("Field type is undefined");
            var parser = this.Parsers[fieldType];
            if (!parser)
                throw new Error("Field type is unknown");
            var field = parser(fieldElem);
            jqField.contents().each(function (elemId, elem) {
                var jqElem = $(elem);
                field.SetCaption(jqElem.attr("caption"));
                field.SetValue(jqElem.attr("value"));
                switch ((elem.tagName || "").toLowerCase()) {
                    case "style":
                        field.Style = {
                            RootStyle: jqElem.attr("root").split(" "),
                            LabelStyle: jqElem.attr("label").split(" "),
                            FieldStyle: jqElem.attr("field").split(" ")
                        };
                        break;
                }
            });
            return field;
        };
        Form.CheckFormElement = function (elemId, elem, fieldHandler, buttonHandler) {
            var jqElem = $(elem);
            switch ((elem.tagName || "").toLowerCase()) {
                case "gps-field":
                    var field = Form.ParseField(elem)
                        .InsertAfter(jqElem);
                    if (fieldHandler) {
                        fieldHandler(field, elem);
                    }
                    jqElem.remove();
                    break;
                case "gps-button":
                    if (!jqElem.attr("link-to-form"))
                        return;
                    var button = Button_1.Button.ParseButton(jqElem);
                    if (buttonHandler) {
                        buttonHandler(button, elem);
                    }
                    break;
                default:
                    $(elem)
                        .contents()
                        .each(function (elemId, elem) { return Form.CheckFormElement(elemId, elem, fieldHandler, buttonHandler); });
                    break;
            }
        };
        Form.Parsers = {};
        return Form;
    }());
    exports.Form = Form;
    var FormField = (function () {
        function FormField(field) {
            this._valuePreparer = function (value) { return value; };
            this._style = {};
            this._label = $("<label/>");
            this._field = field;
            this._element = $("<div/>")
                .append(this._label, this._field);
        }
        Object.defineProperty(FormField.prototype, "Style", {
            get: function () {
                return this._style;
            },
            set: function (style) {
                style = style || {};
                this.ApplyStyle(style);
                this._style = style;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FormField.prototype, "Caption", {
            get: function () {
                return this._label.text();
            },
            enumerable: true,
            configurable: true
        });
        FormField.prototype.SetStyle = function (style) {
            this.Style = style;
            return this;
        };
        FormField.prototype.SetCaption = function (value) {
            this._label.append(value);
            return this;
        };
        FormField.prototype.GetRoot = function () {
            return this._element;
        };
        FormField.prototype.GetValue = function () {
            var value = this.GetValueOverride();
            return this._valuePreparer(value);
        };
        FormField.prototype.SetValuePreparer = function (preparer) {
            this._valuePreparer = preparer || (function (value) { return value; });
            return this;
        };
        FormField.prototype.AppendTo = function (container) {
            container.append(this.GetRoot());
            return this;
        };
        FormField.prototype.InsertAfter = function (elem) {
            this.GetRoot().insertAfter(elem);
            return this;
        };
        Object.defineProperty(FormField.prototype, "Field", {
            get: function () {
                return this._field;
            },
            enumerable: true,
            configurable: true
        });
        FormField.prototype.ApplyStyle = function (style) {
            StyleUtilities_1.ApplyClass(this._style, style, function (s) { return s.RootStyle; }, this._element);
            StyleUtilities_1.ApplyClass(this._style, style, function (s) { return s.LabelStyle; }, this._label);
            StyleUtilities_1.ApplyClass(this._style, style, function (s) { return s.FieldStyle; }, this._field);
        };
        return FormField;
    }());
    exports.FormField = FormField;
});
//# sourceMappingURL=Form.js.map