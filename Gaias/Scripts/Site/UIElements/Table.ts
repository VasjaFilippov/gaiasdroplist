﻿import {Ajax} from "../Utility/Ajax"
import {ElementClasses, ApplyClass, TryAddClass} from "../Utility/StyleUtilities"
import {GaiasEvent, IPublicEvent} from "../Utility/Event"
import {Aggregate} from "../Utility/ArrayUtils";
import {TableValueFormaters} from "../UIElements/TableValueFormaters"

export type ValueGetter<TModel> = (rowData: TModel) => any;

export class PlainTextValueFormater implements ITableValueFormater {
    public CreateCell(table: TableBase, row: number): JQuery {
        return this._cells[row] = $("<span/>");
    }

    public UpdateValue(value: any, row: number): void {
        this._cells[row].empty().append(value);
    }

    private _cells = new Array<JQuery>();
}

export interface ITableValueFormater {
    CreateCell(table: TableBase, row: number): JQuery;
    UpdateValue(value: any, row: number): void;
}

export interface IElementsTable<TElement> {
    [name: string]: TElement;
}

export interface IColumn<TModel> {
    Header: string;
    Getter: ValueGetter<TModel>;
    Visible?: boolean;
    HeaderFormater?: ITableValueFormater;
    ColumnFormater?: ITableValueFormater;
}

export interface ITableSectionStyle {
    Class?: ElementClasses;
    RowClass?: ElementClasses;
    CellClass?: ElementClasses;
}

export interface ITableStyle {
    Header?: ITableSectionStyle;
    Body?: ITableSectionStyle;
    Footer?: ITableSectionStyle;
    Any?: ITableSectionStyle;
    Table?: ElementClasses;
}

export interface ITableData {
// ReSharper disable InconsistentNaming
    rows: any[];
    total: number;
    page: number;
// ReSharper restore InconsistentNaming
}

export class TableSorting {
    public Column: string;
    public SortAsc: boolean;

    constructor(column: string, sortAsc: boolean) {
        this.Column = column;
        this.SortAsc = sortAsc;
    }
}

export class TableFilter {
    public Column: string;
    public Filter: string;

    constructor(column: string, filter: string) {
        this.Column = column;
        this.Filter = filter;
    }
}

export abstract class TableBase {
    public Url: string;

    public get AfterUpdate(): IPublicEvent<TableBase> {
        return this._afterUpdate;
    }

    public get SortsChanged(): IPublicEvent<TableBase> {
        return this._sortsChanged;
    }

    public get Name(): string {
        return this._name;
    }

    public get Sorts(): TableSorting[] {
        return this._sorts;
    }

    public set Sorts(value: TableSorting[]) {
        this._sorts = value;
        this.Update();
        this._sortsChanged.Invoke(this);
    }

    public get Filters(): TableFilter[] {
        return this._filters;
    }

    public set Filters(value: TableFilter[]) {
        this._filters = value;
        this.Update();
        this._filtersChanged.Invoke(this);
    }
    
    public get Style(): ITableStyle {
        return this.StyleProt;
    }

    public set Style(value: ITableStyle) {
        value = value || {};

        this.ApplyStyle(value);
        this.StyleProt = value;
    }

    public get Rows(): number {
        return this.RowsProt.length;
    }

    public set Rows(value: number) {
        if (this.Rows === value)
            return;

        if (this.Rows > value) {
            for (; this.Rows > value;) {
                this.RowsProt.pop().remove();
            }
        } else {
            for (; this.Rows < value;) {
                this.AddRow(this.Rows + 1);
            }
        }

        this.Page = Math.floor((this.Page - 1) * this.Rows / value + 1);
    }

    public get Page(): number {
        return this._page;
    }

    public set Page(value: number) {
        if (this.Page === value)
            return;

        this._page = value;
        this.Update();
    }

    public get Pages() {
        if (!this.Data)
            return 0;

        return this._data.total;
    }

    public get Data(): ITableData {
        return this._data;
    }

    public set Data(value: ITableData) {
        if (!value)
            return;

        this._data = value;
        this._page = value.page;
        this.Refill();
        this._afterUpdate.Invoke(this);
    }

    public constructor(name: string, container: JQuery) {
        Table.TablesTable[name] = this;

        this._name = name;
        this._table = $("<table/>");

        this.Header = $("<thead/>");
        this.Body = $("<tbody/>");
        this.Footer = $("<tfoot/>");

        this._table.append(
            this.Header,
            this.Body,
            this.Footer);

        this._container = container;

        this._container
            .empty()
            .append(this._table);
    }

    public Update(): void {
        if (!this.Url)
            return;

        const ajax = new Ajax(
            this.Url,
            (data) => this.Data = data);

        ajax.Send({
            Page: this.Page,
            PageSize: this.Rows,
            Sorts: this._sorts,
            Filters: this._filters
    });
    }

    public AddSort(sort: TableSorting): void {
        this.Sorts.push(sort);
        this.Update();
    }

    public RemoveSort(column: string): void {
        this.Sorts = this.Sorts.filter(sort => sort.Column !== column);
        this.Update();
    }

    public GetSort(column: string): TableSorting {
        const sorts = this.Sorts;

        for (let sortId in sorts) {
            if (sorts.hasOwnProperty(sortId)) {
                const sort = sorts[sortId];

                if (sort.Column === column)
                    return sort;
            }
        }

        return undefined;
    }

    public UpdateSort(column: string, updater: (sort: TableSorting) => void): void {
        const sort = this.GetSort(column);

        if (!sort)
            return;

        updater(sort);
        this.Update();
    }
    
    protected StyleProt: ITableStyle = {};

    protected RowsProt = new Array<JQuery>();

    protected Header: JQuery;
    protected Body: JQuery;
    protected Footer: JQuery;

    protected Refill(): void {
        if (!this.Data)
            return;

        this.Page = this.Data.page;
        this.RowsProt.forEach((row, rowId) => this.RefillRow(row, rowId));
    }

    private _afterUpdate = new GaiasEvent<TableBase>();
    private _sortsChanged = new GaiasEvent<TableBase>();
    private _filtersChanged = new GaiasEvent<TableBase>();
    private _container: JQuery;
    private _table: JQuery;
    private _sorts: TableSorting[];
    private _filters: TableFilter[];

    private _data: ITableData;
    private _name: string;
    private _page: number;

    private ApplyStyle(style: ITableStyle): void {
        ApplyClass(this.StyleProt, style, s => s.Table, this._table);

        Table.ApplySectionStyle(this.StyleProt, style, s => s.Header, this.Header);
        Table.ApplySectionStyle(this.StyleProt, style, s => s.Any, this.Header);

        Table.ApplySectionStyle(this.StyleProt, style, s => s.Body, this.Body);
        Table.ApplySectionStyle(this.StyleProt, style, s => s.Any, this.Body);

        Table.ApplySectionStyle(this.StyleProt, style, s => s.Footer, this.Footer);
        Table.ApplySectionStyle(this.StyleProt, style, s => s.Any, this.Footer);
    }
    
    protected abstract RefillRow(row: JQuery, rowId: number): void;
    protected abstract AddRow(row: number): void;

    

    public static ApplySectionStyle(oldStyle: ITableStyle, style: ITableStyle, getSubstyle: (style: ITableStyle) => ITableSectionStyle, section: JQuery) {
        const oldSubStyle: ITableSectionStyle = getSubstyle(oldStyle) || {};
        const subStyle: ITableSectionStyle = getSubstyle(style) || {};

        ApplyClass(oldSubStyle, subStyle, s => s.Class, section);
        section.contents().each((rowId, rowElem) => {
            const row = $(rowElem);

            ApplyClass(oldSubStyle, subStyle, s => s.RowClass, row);
            row.contents().each((cellId, cellElem) => {
                ApplyClass(oldSubStyle, subStyle, s => s.CellClass, $(cellElem));
            });
        });
    }

    public static get TablesTable(): IElementsTable<TableBase> {
        (window as any).Tables = (window as any).Tables || {};
        return (window as any).Tables;
    }
}

export class Table<TModel> extends TableBase {
    public get Columns(): IColumn<TModel>[] {
        return this._columns;
    }

    public set Columns(value: IColumn<TModel>[]) {
        value = value || new Array<IColumn<TModel>>();

        value.forEach(col => {
            if (!col.HeaderFormater)
                col.HeaderFormater = new PlainTextValueFormater();

            if (!col.ColumnFormater)
                col.ColumnFormater = new PlainTextValueFormater();
        });

        this.UpdateColumns(value);
        this._columns = value;
        this.AddHeader();
    }

    public constructor(name: string, container: JQuery) {
        super(name, container);
    }
    
    private _columns = new Array<IColumn<TModel>>();
    
    protected RefillRow(row: JQuery, rowId: number): void {
        if (this.Data.rows.length <= rowId) {
            row.hide();
            return;
        }

        const rowData = this.Data.rows[rowId];

        row.show();

        this.Columns.forEach(col => {
            const data = col.Getter(rowData);
            col.ColumnFormater.UpdateValue(data, rowId + 1);
        });
    }

    private UpdateColumns(columns: IColumn<TModel>[]): void {
        const rows = this.Rows;

        this._columns = columns;
        this.Rows = 0;
        this.Rows = rows;
        this.Refill();
    }

    private AddHeader(): void {
        const headerRow = $("<tr/>");
        const styles = [this.StyleProt.Header, this.StyleProt.Any];

        this._columns.forEach(col => {
            const cell = $("<td/>");
            const content = col.HeaderFormater.CreateCell(this, 0);

            col.HeaderFormater.UpdateValue(col.Header, 0);

            if (col.Visible === false) {
                cell.hide();
            }
            
            cell
                .empty()
                .append(content);

            TryAddClass(cell, styles, s => s.CellClass);
            headerRow.append(cell);
        });

        TryAddClass(headerRow, styles, s => s.RowClass);
        this.Header
            .empty()
            .append(headerRow);
    }

    protected AddRow(rowId: number): void {
        const row = $("<tr/>");
        const styles = [this.StyleProt.Any, this.StyleProt.Body];

        this._columns.forEach(col => {
            const cell = $("<td/>");
            const content = col.ColumnFormater.CreateCell(this, rowId);

            if (col.Visible === false) {
                cell.hide();
            }

            TryAddClass(cell, styles, s => s.CellClass);
            cell.append(content);
            row.append(cell);
        });

        TryAddClass(row, styles, s => s.RowClass);
        this.Body.append(row);
        this.RowsProt.push(row);
    }
}

$(document).ready(() => $("gps-grid").each((gridId, grid) => {
    const jqGrid = $(grid);
    const container = $("<div/>").insertAfter(jqGrid);
    const table = new Table<any>(jqGrid.attr("name"), container);
    const columns = new Array<IColumn<any>>();
    const styles: { [section: string]: ITableSectionStyle } = {};

    function ParseSortState(sortState: string): TableValueFormaters.SortState {
        switch (sortState) {
            case "asc":
                return TableValueFormaters.SortState.Ascending;
            case "desc":
                return TableValueFormaters.SortState.Descending;
            default:
                return TableValueFormaters.SortState.None;
        }
    }

    function TryGetFunction(jqElem: JQuery, args: string, defaultValue: any): any {
        if (!jqElem || jqElem.length === 0)
            return defaultValue;

        return new Function(args, `return ${jqElem.html()};`);
    }

    function GetGetter(jqCol: JQuery): any {
        switch (jqCol.attr("getter")) {
            case "aggregate":
                const arrayGetter = new Function("row", `return ${jqCol.children("array").html()};`);
                const aggregator = TryGetFunction(jqCol.children("aggregator"), "row,prev,next", (row: any, prev: any, next: any) => prev + next);
                const elemConvertor = TryGetFunction(jqCol.children("elem-converter"), "row,elem", (row: any, elem: any) => elem);
                const init = TryGetFunction(jqCol.children("init"), "row", (): any => null);
                const convertor = TryGetFunction(jqCol.children("convertor"), "row,result", (row: any, result: any) => result);

                return (row: any) => Aggregate<any, any, any, any>(
                    arrayGetter(row),
                    (prev, next) => aggregator(row, prev, next),
                    elem => elemConvertor(row, elem),
                    init(row),
                    result => convertor(row, result)
                );
            default:
                return new Function("row", `return ${jqCol.html()}`);
        }
    }

    function ParseColumn(colElem: Element): IColumn<any> {
        const jqCol = $(colElem);
        const name = jqCol.attr("name");
        const header = jqCol.attr("header") || name;
        const sort = jqCol.attr("sort");
        const visible = jqCol.attr("visible") ? jqCol.attr("visible") === "true" : true;
        const getter = GetGetter(jqCol);

        const headerFormatter = sort
            ? new TableValueFormaters.SorterPlainTextFormater(name, ParseSortState(sort))
            : undefined;

        return {
            Header: header,
            Getter: getter,
            Visible: visible,
            HeaderFormater: headerFormatter
        };
    }

    function ParseStyles(styleElem: Element): void {
        const jqStyle = $(styleElem);
        const style: ITableSectionStyle = {};

        style.Class = jqStyle.attr("class").split(" ");
        style.CellClass = jqStyle.attr("cell-class").split(" ");
        style.RowClass = jqStyle.attr("row-class").split(" ");

        styles[jqStyle.attr("section")] = style;
    }

    jqGrid.children().each((childId, child) => {
        if (!child.tagName)
            return;

        switch (child.tagName.toLowerCase()) {
            case "column":
                columns.push(ParseColumn(child));
                break;
            case "section-style":
                ParseStyles(child);
                break;
        }
    });

    const tableClasses = jqGrid.attr("class").split(" ");

    table.Columns = columns;
    table.Url = jqGrid.attr("url");
    table.Style = {
        Table: tableClasses,
        Any: styles["any"],
        Body: styles["body"],
        Header: styles["header"],
        Footer: styles["footer"]
    };
    table.Rows = +jqGrid.attr("rows") || 25;

    jqGrid.remove();
}));