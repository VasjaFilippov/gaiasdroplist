define(["require", "exports", "./Form/Form", "./Table", "../Utility/JqUtils"], function (require, exports, Form_1, Table_1, JqUtils_1) {
    "use strict";
    var _this = this;
    var TableFilterField = (function () {
        function TableFilterField(column, field) {
            this._column = column;
            this._field = field;
        }
        Object.defineProperty(TableFilterField.prototype, "Column", {
            get: function () {
                return this._column;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TableFilterField.prototype, "Field", {
            get: function () {
                return this._field;
            },
            enumerable: true,
            configurable: true
        });
        return TableFilterField;
    }());
    exports.TableFilterField = TableFilterField;
    $(document).ready(function () { return $("gps-filter-form").each(function (id, formElem) {
        var jqForm = $(formElem);
        var table = Table_1.TableBase.TablesTable[jqForm.attr("table")];
        var filters = new Array();
        var appliers = new Array();
        Form_1.Form.ParseForm(jqForm, function (field, elem) {
            var jqElem = $(elem);
            var column = jqElem.attr("column");
            if (field.Caption.length === 0) {
                field.SetCaption(column);
            }
            filters.push(new TableFilterField(column, field));
        }, function (button) { return appliers.push(button); });
        appliers.forEach(function (applier) {
            return applier.Click.AddHandler(function () {
                var tableFilters = new Array();
                filters.forEach(function (filter) {
                    var value = filter.Field.GetValue();
                    if (value.length > 0 && value != -1) {
                        tableFilters.push(new Table_1.TableFilter(filter.Column, value));
                    }
                });
                table.Filters = tableFilters;
            }, _this);
        });
        jqForm.attr("table", undefined);
        JqUtils_1.ChangeTag(formElem, "div");
    }); });
});
//# sourceMappingURL=TableFilters.js.map