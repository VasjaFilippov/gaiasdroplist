var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "../Utility/Ajax", "../Utility/StyleUtilities", "../Utility/Event", "../Utility/ArrayUtils", "../UIElements/TableValueFormaters"], function (require, exports, Ajax_1, StyleUtilities_1, Event_1, ArrayUtils_1, TableValueFormaters_1) {
    "use strict";
    var PlainTextValueFormater = (function () {
        function PlainTextValueFormater() {
            this._cells = new Array();
        }
        PlainTextValueFormater.prototype.CreateCell = function (table, row) {
            return this._cells[row] = $("<span/>");
        };
        PlainTextValueFormater.prototype.UpdateValue = function (value, row) {
            this._cells[row].empty().append(value);
        };
        return PlainTextValueFormater;
    }());
    exports.PlainTextValueFormater = PlainTextValueFormater;
    var TableSorting = (function () {
        function TableSorting(column, sortAsc) {
            this.Column = column;
            this.SortAsc = sortAsc;
        }
        return TableSorting;
    }());
    exports.TableSorting = TableSorting;
    var TableFilter = (function () {
        function TableFilter(column, filter) {
            this.Column = column;
            this.Filter = filter;
        }
        return TableFilter;
    }());
    exports.TableFilter = TableFilter;
    var TableBase = (function () {
        function TableBase(name, container) {
            this.StyleProt = {};
            this.RowsProt = new Array();
            this._afterUpdate = new Event_1.GaiasEvent();
            this._sortsChanged = new Event_1.GaiasEvent();
            this._filtersChanged = new Event_1.GaiasEvent();
            Table.TablesTable[name] = this;
            this._name = name;
            this._table = $("<table/>");
            this.Header = $("<thead/>");
            this.Body = $("<tbody/>");
            this.Footer = $("<tfoot/>");
            this._table.append(this.Header, this.Body, this.Footer);
            this._container = container;
            this._container
                .empty()
                .append(this._table);
        }
        Object.defineProperty(TableBase.prototype, "AfterUpdate", {
            get: function () {
                return this._afterUpdate;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TableBase.prototype, "SortsChanged", {
            get: function () {
                return this._sortsChanged;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TableBase.prototype, "Name", {
            get: function () {
                return this._name;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TableBase.prototype, "Sorts", {
            get: function () {
                return this._sorts;
            },
            set: function (value) {
                this._sorts = value;
                this.Update();
                this._sortsChanged.Invoke(this);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TableBase.prototype, "Filters", {
            get: function () {
                return this._filters;
            },
            set: function (value) {
                this._filters = value;
                this.Update();
                this._filtersChanged.Invoke(this);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TableBase.prototype, "Style", {
            get: function () {
                return this.StyleProt;
            },
            set: function (value) {
                value = value || {};
                this.ApplyStyle(value);
                this.StyleProt = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TableBase.prototype, "Rows", {
            get: function () {
                return this.RowsProt.length;
            },
            set: function (value) {
                if (this.Rows === value)
                    return;
                if (this.Rows > value) {
                    for (; this.Rows > value;) {
                        this.RowsProt.pop().remove();
                    }
                }
                else {
                    for (; this.Rows < value;) {
                        this.AddRow(this.Rows + 1);
                    }
                }
                this.Page = Math.floor((this.Page - 1) * this.Rows / value + 1);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TableBase.prototype, "Page", {
            get: function () {
                return this._page;
            },
            set: function (value) {
                if (this.Page === value)
                    return;
                this._page = value;
                this.Update();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TableBase.prototype, "Pages", {
            get: function () {
                if (!this.Data)
                    return 0;
                return this._data.total;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TableBase.prototype, "Data", {
            get: function () {
                return this._data;
            },
            set: function (value) {
                if (!value)
                    return;
                this._data = value;
                this._page = value.page;
                this.Refill();
                this._afterUpdate.Invoke(this);
            },
            enumerable: true,
            configurable: true
        });
        TableBase.prototype.Update = function () {
            var _this = this;
            if (!this.Url)
                return;
            var ajax = new Ajax_1.Ajax(this.Url, function (data) { return _this.Data = data; });
            ajax.Send({
                Page: this.Page,
                PageSize: this.Rows,
                Sorts: this._sorts,
                Filters: this._filters
            });
        };
        TableBase.prototype.AddSort = function (sort) {
            this.Sorts.push(sort);
            this.Update();
        };
        TableBase.prototype.RemoveSort = function (column) {
            this.Sorts = this.Sorts.filter(function (sort) { return sort.Column !== column; });
            this.Update();
        };
        TableBase.prototype.GetSort = function (column) {
            var sorts = this.Sorts;
            for (var sortId in sorts) {
                if (sorts.hasOwnProperty(sortId)) {
                    var sort = sorts[sortId];
                    if (sort.Column === column)
                        return sort;
                }
            }
            return undefined;
        };
        TableBase.prototype.UpdateSort = function (column, updater) {
            var sort = this.GetSort(column);
            if (!sort)
                return;
            updater(sort);
            this.Update();
        };
        TableBase.prototype.Refill = function () {
            var _this = this;
            if (!this.Data)
                return;
            this.Page = this.Data.page;
            this.RowsProt.forEach(function (row, rowId) { return _this.RefillRow(row, rowId); });
        };
        TableBase.prototype.ApplyStyle = function (style) {
            StyleUtilities_1.ApplyClass(this.StyleProt, style, function (s) { return s.Table; }, this._table);
            Table.ApplySectionStyle(this.StyleProt, style, function (s) { return s.Header; }, this.Header);
            Table.ApplySectionStyle(this.StyleProt, style, function (s) { return s.Any; }, this.Header);
            Table.ApplySectionStyle(this.StyleProt, style, function (s) { return s.Body; }, this.Body);
            Table.ApplySectionStyle(this.StyleProt, style, function (s) { return s.Any; }, this.Body);
            Table.ApplySectionStyle(this.StyleProt, style, function (s) { return s.Footer; }, this.Footer);
            Table.ApplySectionStyle(this.StyleProt, style, function (s) { return s.Any; }, this.Footer);
        };
        TableBase.ApplySectionStyle = function (oldStyle, style, getSubstyle, section) {
            var oldSubStyle = getSubstyle(oldStyle) || {};
            var subStyle = getSubstyle(style) || {};
            StyleUtilities_1.ApplyClass(oldSubStyle, subStyle, function (s) { return s.Class; }, section);
            section.contents().each(function (rowId, rowElem) {
                var row = $(rowElem);
                StyleUtilities_1.ApplyClass(oldSubStyle, subStyle, function (s) { return s.RowClass; }, row);
                row.contents().each(function (cellId, cellElem) {
                    StyleUtilities_1.ApplyClass(oldSubStyle, subStyle, function (s) { return s.CellClass; }, $(cellElem));
                });
            });
        };
        Object.defineProperty(TableBase, "TablesTable", {
            get: function () {
                window.Tables = window.Tables || {};
                return window.Tables;
            },
            enumerable: true,
            configurable: true
        });
        return TableBase;
    }());
    exports.TableBase = TableBase;
    var Table = (function (_super) {
        __extends(Table, _super);
        function Table(name, container) {
            _super.call(this, name, container);
            this._columns = new Array();
        }
        Object.defineProperty(Table.prototype, "Columns", {
            get: function () {
                return this._columns;
            },
            set: function (value) {
                value = value || new Array();
                value.forEach(function (col) {
                    if (!col.HeaderFormater)
                        col.HeaderFormater = new PlainTextValueFormater();
                    if (!col.ColumnFormater)
                        col.ColumnFormater = new PlainTextValueFormater();
                });
                this.UpdateColumns(value);
                this._columns = value;
                this.AddHeader();
            },
            enumerable: true,
            configurable: true
        });
        Table.prototype.RefillRow = function (row, rowId) {
            if (this.Data.rows.length <= rowId) {
                row.hide();
                return;
            }
            var rowData = this.Data.rows[rowId];
            row.show();
            this.Columns.forEach(function (col) {
                var data = col.Getter(rowData);
                col.ColumnFormater.UpdateValue(data, rowId + 1);
            });
        };
        Table.prototype.UpdateColumns = function (columns) {
            var rows = this.Rows;
            this._columns = columns;
            this.Rows = 0;
            this.Rows = rows;
            this.Refill();
        };
        Table.prototype.AddHeader = function () {
            var _this = this;
            var headerRow = $("<tr/>");
            var styles = [this.StyleProt.Header, this.StyleProt.Any];
            this._columns.forEach(function (col) {
                var cell = $("<td/>");
                var content = col.HeaderFormater.CreateCell(_this, 0);
                col.HeaderFormater.UpdateValue(col.Header, 0);
                if (col.Visible === false) {
                    cell.hide();
                }
                cell
                    .empty()
                    .append(content);
                StyleUtilities_1.TryAddClass(cell, styles, function (s) { return s.CellClass; });
                headerRow.append(cell);
            });
            StyleUtilities_1.TryAddClass(headerRow, styles, function (s) { return s.RowClass; });
            this.Header
                .empty()
                .append(headerRow);
        };
        Table.prototype.AddRow = function (rowId) {
            var _this = this;
            var row = $("<tr/>");
            var styles = [this.StyleProt.Any, this.StyleProt.Body];
            this._columns.forEach(function (col) {
                var cell = $("<td/>");
                var content = col.ColumnFormater.CreateCell(_this, rowId);
                if (col.Visible === false) {
                    cell.hide();
                }
                StyleUtilities_1.TryAddClass(cell, styles, function (s) { return s.CellClass; });
                cell.append(content);
                row.append(cell);
            });
            StyleUtilities_1.TryAddClass(row, styles, function (s) { return s.RowClass; });
            this.Body.append(row);
            this.RowsProt.push(row);
        };
        return Table;
    }(TableBase));
    exports.Table = Table;
    $(document).ready(function () { return $("gps-grid").each(function (gridId, grid) {
        var jqGrid = $(grid);
        var container = $("<div/>").insertAfter(jqGrid);
        var table = new Table(jqGrid.attr("name"), container);
        var columns = new Array();
        var styles = {};
        function ParseSortState(sortState) {
            switch (sortState) {
                case "asc":
                    return 1 /* Ascending */;
                case "desc":
                    return 2 /* Descending */;
                default:
                    return 0 /* None */;
            }
        }
        function TryGetFunction(jqElem, args, defaultValue) {
            if (!jqElem || jqElem.length === 0)
                return defaultValue;
            return new Function(args, "return " + jqElem.html() + ";");
        }
        function GetGetter(jqCol) {
            switch (jqCol.attr("getter")) {
                case "aggregate":
                    var arrayGetter_1 = new Function("row", "return " + jqCol.children("array").html() + ";");
                    var aggregator_1 = TryGetFunction(jqCol.children("aggregator"), "row,prev,next", function (row, prev, next) { return prev + next; });
                    var elemConvertor_1 = TryGetFunction(jqCol.children("elem-converter"), "row,elem", function (row, elem) { return elem; });
                    var init_1 = TryGetFunction(jqCol.children("init"), "row", function () { return null; });
                    var convertor_1 = TryGetFunction(jqCol.children("convertor"), "row,result", function (row, result) { return result; });
                    return function (row) { return ArrayUtils_1.Aggregate(arrayGetter_1(row), function (prev, next) { return aggregator_1(row, prev, next); }, function (elem) { return elemConvertor_1(row, elem); }, init_1(row), function (result) { return convertor_1(row, result); }); };
                default:
                    return new Function("row", "return " + jqCol.html());
            }
        }
        function ParseColumn(colElem) {
            var jqCol = $(colElem);
            var name = jqCol.attr("name");
            var header = jqCol.attr("header") || name;
            var sort = jqCol.attr("sort");
            var visible = jqCol.attr("visible") ? jqCol.attr("visible") === "true" : true;
            var getter = GetGetter(jqCol);
            var headerFormatter = sort
                ? new TableValueFormaters_1.TableValueFormaters.SorterPlainTextFormater(name, ParseSortState(sort))
                : undefined;
            return {
                Header: header,
                Getter: getter,
                Visible: visible,
                HeaderFormater: headerFormatter
            };
        }
        function ParseStyles(styleElem) {
            var jqStyle = $(styleElem);
            var style = {};
            style.Class = jqStyle.attr("class").split(" ");
            style.CellClass = jqStyle.attr("cell-class").split(" ");
            style.RowClass = jqStyle.attr("row-class").split(" ");
            styles[jqStyle.attr("section")] = style;
        }
        jqGrid.children().each(function (childId, child) {
            if (!child.tagName)
                return;
            switch (child.tagName.toLowerCase()) {
                case "column":
                    columns.push(ParseColumn(child));
                    break;
                case "section-style":
                    ParseStyles(child);
                    break;
            }
        });
        var tableClasses = jqGrid.attr("class").split(" ");
        table.Columns = columns;
        table.Url = jqGrid.attr("url");
        table.Style = {
            Table: tableClasses,
            Any: styles["any"],
            Body: styles["body"],
            Header: styles["header"],
            Footer: styles["footer"]
        };
        table.Rows = +jqGrid.attr("rows") || 25;
        jqGrid.remove();
    }); });
});
//# sourceMappingURL=Table.js.map