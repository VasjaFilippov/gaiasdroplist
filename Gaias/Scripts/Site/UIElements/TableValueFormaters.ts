﻿import {ITableValueFormater, TableBase, TableSorting} from "./Table"

export namespace TableValueFormaters {
    export const enum SortState {
        None,
        Ascending,
        Descending,
    }

    export class SorterPlainTextFormater implements ITableValueFormater {
        constructor(column: string, defaultSort: SortState = SortState.None) {
            this._column = column;
            this._state = defaultSort;
        }

        public CreateCell(table: TableBase, row: number): JQuery {
            this._arrow = $("<span/>");
            this._text = $("<span/>")
                .css("text-decoration", "underline");

            const root = $("<div/>")
                .append(this._text, this._arrow)
                .click(() => ApplySort(table, this._column, NextSort(this._state)));

            table.SortsChanged.AddHandler(() => {
                const sorting = table.GetSort(this._column);
                let newState: SortState;

                if (sorting) {
                    if (sorting.SortAsc) {
                        newState = SortState.Ascending;
                    } else {
                        newState = SortState.Descending;
                    }
                } else {
                    newState = SortState.None;
                }

                if (newState !== this._state) {
                    this._state = newState;
                    ApplyArrow(this._arrow, this._state);
                }
            }, this);
            
            if (this._state !== SortState.None) {
                ApplySort(table, this._column, this._state);
                ApplyArrow(this._arrow, this._state);
            }

            return root;
        }

        public UpdateValue(value: any, row: number): void {
            this._text.text(value);
        }

        private _column: string;
        private _state: SortState;
        private _text: JQuery;
        private _arrow: JQuery;
    }

    function ApplySort(table: TableBase, column: string, state: SortState): void {
        switch (state) {
            case SortState.None:
                table.RemoveSort(column);
                break;
            case SortState.Ascending:
                table.Sorts = [new TableSorting(column, true)];
                break;
            case SortState.Descending:
                table.Sorts = [new TableSorting(column, false)];
                break;
        }
    }

    function ApplyArrow(arrow: JQuery, state: SortState): void {
        switch (state) {
            case SortState.None:
                arrow.text("");
                break;
            case SortState.Ascending:
                arrow.text("↓");
                break;
            case SortState.Descending:
                arrow.text("↑");
                break;
        }
    }

    function NextSort(sort: SortState): SortState {
        switch (sort) {
            case SortState.Ascending:
                return SortState.Descending;
            case SortState.None:
            case SortState.Descending:
            default:
                return SortState.Ascending;
        }
    }
}