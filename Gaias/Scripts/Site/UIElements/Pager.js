define(["require", "exports", "./Table"], function (require, exports, Table_1) {
    "use strict";
    var Pager = (function () {
        function Pager(params) {
            var _this = this;
            Pager.PagersTable[params.Name] = this;
            this._name = params.Name;
            this._container = params.Container;
            if (typeof params.Table === "string") {
                this._table = Table_1.TableBase.TablesTable[params.Table];
            }
            else {
                this._table = params.Table;
            }
            this._pager = $("<ul>")
                .addClass("pager");
            this._container.append(this._pager);
            this.PagesLimit = params.PageLimit;
            this._table.AfterUpdate.AddHandler(function () { return Pager.PagersTable[_this._name].Update(); }, this);
        }
        Object.defineProperty(Pager.prototype, "Name", {
            get: function () {
                return this._name;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Pager.prototype, "PagesLimit", {
            get: function () {
                return this._pagesLimit;
            },
            set: function (value) {
                if (this.PagesLimit === value)
                    return;
                this._pagesLimit = value;
                this.Update();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Pager.prototype, "Page", {
            get: function () {
                return this._table.Page;
            },
            set: function (value) {
                this._table.Page = value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Pager.prototype, "Pages", {
            get: function () {
                return this._table.Pages;
            },
            enumerable: true,
            configurable: true
        });
        Pager.prototype.GoToNext = function () {
            if (this.Page < this.Pages) {
                this.Page++;
            }
        };
        Pager.prototype.GoToPrev = function () {
            if (this.Page > 1) {
                this.Page--;
            }
        };
        Pager.prototype.GoToLast = function () {
            if (this.Page < this.Pages) {
                this.Page = this.Pages;
            }
        };
        Pager.prototype.GoToFirst = function () {
            if (this.Page > 1) {
                this.Page = 1;
            }
        };
        Pager.prototype.Update = function () {
            var _this = this;
            this._pager.empty();
            var min = this.Page - this.PagesLimit;
            var max = this.Page + this.PagesLimit;
            var firstA = $("<a/>")
                .text("First");
            var first = $("<li/>")
                .addClass("previous")
                .append(firstA);
            var prevA = $("<a/>")
                .text("Previous");
            var prev = $("<li/>")
                .addClass("previous")
                .append(prevA);
            var nextA = $("<a/>")
                .text("Next");
            var next = $("<li/>")
                .addClass("next")
                .append(nextA);
            var lastA = $("<a/>")
                .text("Last");
            var last = $("<li/>")
                .addClass("next")
                .append(lastA);
            this._pager
                .append(first)
                .append(prev);
            if (min < 1) {
                min = 1;
                first.addClass("disabled");
            }
            else {
                firstA
                    .attr("href", "javascript:void(0)")
                    .click(function () { return Pager.PagersTable[_this._name].Page = 1; });
            }
            if (this.Page < 2) {
                prev.addClass("disabled");
            }
            else {
                prevA
                    .attr("href", "javascript:void(0)")
                    .click(function () { return Pager.PagersTable[_this._name].Page = _this.Page - 1; });
            }
            if (max > this.Pages) {
                max = this.Pages;
                last.addClass("disabled");
            }
            else {
                lastA
                    .attr("href", "javascript:void(0)")
                    .click(function () { return Pager.PagersTable[_this._name].Page = _this.Pages; });
            }
            if (this.Page + 1 > this.Pages) {
                next.addClass("disabled");
            }
            else {
                nextA
                    .attr("href", "javascript:void(0)")
                    .click(function () { return Pager.PagersTable[_this._name].Page = _this.Page + 1; });
            }
            var _loop_1 = function(i) {
                var elemA = $("<a/>")
                    .text(i);
                var elem = $("<li/>")
                    .append(elemA);
                if (i === this_1.Page) {
                    elem.addClass("disabled");
                }
                else {
                    elemA
                        .attr("href", "javascript:void(0)")
                        .click(function () { return Pager.PagersTable[_this._name].Page = i; });
                }
                this_1._pager.append(elem);
            };
            var this_1 = this;
            for (var i = min; i <= max; i++) {
                _loop_1(i);
            }
            this._pager
                .append(last)
                .append(next);
        };
        Object.defineProperty(Pager, "PagersTable", {
            get: function () {
                window.Pagers = window.Pagers || {};
                return window.Pagers;
            },
            enumerable: true,
            configurable: true
        });
        return Pager;
    }());
    exports.Pager = Pager;
    $(document).ready(function () { return $("gps-pager").each(function (id, pagerElem) {
        var jqPager = $(pagerElem);
        var container = $("<div/>");
        var pager = new Pager({
            Name: jqPager.attr("name"),
            Container: container,
            PageLimit: +jqPager.attr("page-limit") || 5,
            Table: jqPager.attr("table")
        });
        container.insertAfter(jqPager);
        jqPager.remove();
    }); });
});
//# sourceMappingURL=Pager.js.map