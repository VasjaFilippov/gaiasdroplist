define(["require", "exports"], function (require, exports) {
    "use strict";
    function Aggregate(array, aggregator, elemConverter, init, convertor) {
        if (aggregator === void 0) { aggregator = function (prev, next) { return prev + next; }; }
        if (elemConverter === void 0) { elemConverter = function (elem) { return elem; }; }
        if (init === void 0) { init = null; }
        if (convertor === void 0) { convertor = function (res) { return res; }; }
        var result = init;
        array.forEach(function (elem) { return result = aggregator(result, elemConverter(elem)); });
        return convertor(result);
    }
    exports.Aggregate = Aggregate;
});
//# sourceMappingURL=ArrayUtils.js.map