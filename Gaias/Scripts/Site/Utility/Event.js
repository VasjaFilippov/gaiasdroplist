define(["require", "exports"], function (require, exports) {
    "use strict";
    var EventHandlerData = (function () {
        function EventHandlerData(handler, context) {
            if (context === void 0) { context = window; }
            this.Handler = handler;
            this.Context = context;
        }
        return EventHandlerData;
    }());
    exports.EventHandlerData = EventHandlerData;
    var GaiasEvent = (function () {
        function GaiasEvent() {
            this._handlers = [];
        }
        GaiasEvent.prototype.AddHandler = function (handler, context) {
            if (context === void 0) { context = window; }
            this._handlers.push(new EventHandlerData(handler, context));
        };
        GaiasEvent.prototype.RemoveHandler = function (handler) {
            this._handlers = this._handlers.filter(function (h) { return h.Handler !== handler; });
        };
        GaiasEvent.prototype.Invoke = function (data) {
            this._handlers.slice(0).forEach(function (h) { return h.Handler.call(h.Context, data); });
        };
        return GaiasEvent;
    }());
    exports.GaiasEvent = GaiasEvent;
});
//# sourceMappingURL=Event.js.map