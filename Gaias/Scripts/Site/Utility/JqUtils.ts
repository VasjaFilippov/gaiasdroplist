﻿export function ChangeTag(oldTag: Element, newTag: string, attrHandler?: (attr: Attr) => boolean): JQuery {
    const jq = $(oldTag);
    const newJq = $(`<${newTag}/>`);

    for (let attrId = 0; attrId < oldTag.attributes.length; attrId++) {
        const attr = oldTag.attributes[attrId];
        if (!attrHandler || attrHandler(attr)) {
            newJq.attr(attr.name, attr.value);
        }
    }

    jq  .wrapInner(newJq)
        .children()
        .first()
        .unwrap();

    return newJq;
}