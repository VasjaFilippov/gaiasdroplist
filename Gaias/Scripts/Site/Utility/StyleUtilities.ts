﻿export type ElementClasses = string[] | string;

export function TryAddClass<TStyle>(element: JQuery, styles: TStyle[], getProperty: (style: TStyle) => ElementClasses): void {
    styles.forEach(style => {
        if (!style)
            return;

        const classes = getProperty(style);

        if (!classes)
            return;

        PrepareClasses(classes).forEach(cls => {
            element.addClass(cls);
        });
    });
}

export function ApplyClass<TStyle>(oldStyle: TStyle, style: TStyle, getClass: (style: TStyle) => ElementClasses, element: JQuery): void {
    const applier = GetClassApplier(oldStyle, style, getClass);

    applier(element);
}

export function GetClassApplier<TStyle>(oldStyle: TStyle, style: TStyle, getClass: (style: TStyle) => ElementClasses): (element: JQuery) => void {
    return element => {
        const oldClasses = getClass(oldStyle);
        const classes = getClass(style);

        PrepareClasses(oldClasses).forEach(oldClass => {
            if (oldClass && element.hasClass(oldClass)) {
                element.removeClass(oldClass);
            }
        });

        PrepareClasses(classes).forEach(cls => {
            if (cls && !element.hasClass(cls)) {
                element.addClass(cls);
            }
        });
    };
}

export function PrepareClasses(classes: ElementClasses): string[] {
    if (classes === null || classes === undefined)
        return [];

    if (typeof classes === "string")
        return [classes];
    else
        return classes;
}