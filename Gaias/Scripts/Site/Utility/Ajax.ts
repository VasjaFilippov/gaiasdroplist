﻿export class Ajax{
    constructor(
        public Url: string,
        public Success?: (data: any, textStatus: string, jqXHR: JQueryXHR) => any,
        public Error?: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => any,
        public Type: string = "POST") {
        
    }

    public Send(data?: any) {
        $.ajax({
            type: this.Type,
            url: this.Url,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            dataType: "json",
            success: this.Success,
            error: this.Error
        });
    }
}