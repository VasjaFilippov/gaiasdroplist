﻿export function Aggregate<TRawElement, TElement, TAccumulate, TResult>(
        array: TRawElement[],
        aggregator: (prev: TAccumulate, next: TElement) => TAccumulate = (prev, next) => prev as any + next,
        elemConverter: (elem: TRawElement) => TElement = elem => elem as any,
        init: TAccumulate = null,
        convertor: (result: TAccumulate) => TResult = res => res as any): TResult {
    let result = init;

    array.forEach(elem => result = aggregator(result, elemConverter(elem)));

    return convertor(result);
}