define(["require", "exports"], function (require, exports) {
    "use strict";
    var Ajax = (function () {
        function Ajax(Url, Success, Error, Type) {
            if (Type === void 0) { Type = "POST"; }
            this.Url = Url;
            this.Success = Success;
            this.Error = Error;
            this.Type = Type;
        }
        Ajax.prototype.Send = function (data) {
            $.ajax({
                type: this.Type,
                url: this.Url,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data),
                dataType: "json",
                success: this.Success,
                error: this.Error
            });
        };
        return Ajax;
    }());
    exports.Ajax = Ajax;
});
//# sourceMappingURL=Ajax.js.map