define(["require", "exports"], function (require, exports) {
    "use strict";
    function TryAddClass(element, styles, getProperty) {
        styles.forEach(function (style) {
            if (!style)
                return;
            var classes = getProperty(style);
            if (!classes)
                return;
            PrepareClasses(classes).forEach(function (cls) {
                element.addClass(cls);
            });
        });
    }
    exports.TryAddClass = TryAddClass;
    function ApplyClass(oldStyle, style, getClass, element) {
        var applier = GetClassApplier(oldStyle, style, getClass);
        applier(element);
    }
    exports.ApplyClass = ApplyClass;
    function GetClassApplier(oldStyle, style, getClass) {
        return function (element) {
            var oldClasses = getClass(oldStyle);
            var classes = getClass(style);
            PrepareClasses(oldClasses).forEach(function (oldClass) {
                if (oldClass && element.hasClass(oldClass)) {
                    element.removeClass(oldClass);
                }
            });
            PrepareClasses(classes).forEach(function (cls) {
                if (cls && !element.hasClass(cls)) {
                    element.addClass(cls);
                }
            });
        };
    }
    exports.GetClassApplier = GetClassApplier;
    function PrepareClasses(classes) {
        if (classes === null || classes === undefined)
            return [];
        if (typeof classes === "string")
            return [classes];
        else
            return classes;
    }
    exports.PrepareClasses = PrepareClasses;
});
//# sourceMappingURL=StyleUtilities.js.map