define(["require", "exports"], function (require, exports) {
    "use strict";
    function ChangeTag(oldTag, newTag, attrHandler) {
        var jq = $(oldTag);
        var newJq = $("<" + newTag + "/>");
        for (var attrId = 0; attrId < oldTag.attributes.length; attrId++) {
            var attr = oldTag.attributes[attrId];
            if (!attrHandler || attrHandler(attr)) {
                newJq.attr(attr.name, attr.value);
            }
        }
        jq.wrapInner(newJq)
            .children()
            .first()
            .unwrap();
        return newJq;
    }
    exports.ChangeTag = ChangeTag;
});
//# sourceMappingURL=JqUtils.js.map