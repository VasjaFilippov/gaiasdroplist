﻿export type EventHandler<TArg> = { (data?: TArg): void; };

export interface IPublicEvent<TArg> {
    AddHandler(handler: EventHandler<TArg>, context: any): void;
    RemoveHandler(handler: EventHandler<TArg>): void;
}

export class EventHandlerData<TArg> {
    public Handler: EventHandler<TArg>;
    public Context: any;

    constructor(handler: EventHandler<TArg>, context: any = window) {
        this.Handler = handler;
        this.Context = context;
    }
}

export class GaiasEvent<TArg> implements IPublicEvent<TArg> {
    private _handlers: EventHandlerData<TArg>[] = [];

    public AddHandler(handler: EventHandler<TArg>, context: any = window): void {
        this._handlers.push(new EventHandlerData(handler, context));
    }

    public RemoveHandler(handler: EventHandler<TArg>): void {
        this._handlers = this._handlers.filter(h => h.Handler !== handler);
    }

    public Invoke(data?: TArg): void {
        this._handlers.slice(0).forEach(h => h.Handler.call(h.Context, data));
    }
}