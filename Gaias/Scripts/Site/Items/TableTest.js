var TableTest = (function () {
    function TableTest() {
    }
    TableTest.prototype.Success = function (data) {
        alert(JSON.stringify(data));
    };
    ;
    TableTest.prototype.Error = function (jqxhr, textStatus, errorThrown) {
        alert("error: " + errorThrown + "\r\nstatus: " + textStatus);
    };
    ;
    TableTest.prototype.Run = function (url) {
        var ajax = new Ajax(url, this.Success, this.Error);
        ajax.Send({
            page: 2,
            pageSize: 20,
            sorts: [
                {
                    column: "Rarity",
                    sortAsc: false
                },
                {
                    column: "Name",
                    sortAsc: true
                }
            ]
        });
    };
    return TableTest;
}());
//# sourceMappingURL=TableTest.js.map