var ItemsTable = (function () {
    function ItemsTable(params) {
        $(document).ready(function () {
            var itemsTable = new Table($("#items-table"));
            itemsTable.Url = params.Url;
            itemsTable.Rows = 25;
            itemsTable.Columns = [
                {
                    Getter: function (row) { return row.id; },
                    Header: "Id",
                    Visible: false
                },
                {
                    Getter: function (row) { return row.name; },
                    Header: "Name"
                }
            ];
        });
    }
    return ItemsTable;
}());
//# sourceMappingURL=Items.js.map