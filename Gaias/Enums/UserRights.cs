﻿using System;

namespace Gaias.Enums
{
    [Flags]
    public enum UserRights
    {
        None = 0,
        ViewEntities = 1,
        AddEntities = ViewEntities << 1,
        EditEntities = AddEntities << 1,
        RemoveEntities = EditEntities << 1,
        ChangeUserRights = RemoveEntities << 1,
    }
}