﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Gaias.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ErrorCodes
    {
        [EnumMember(Value = "ItemAlreadyExists")]
        ItemAlreadyExists,
        [EnumMember(Value = "ItemTypeNotFound")]
        ItemTypeNotFound,
        [EnumMember(Value = "ItemRarityNotFound")]
        ItemRarityNotFound,
        [EnumMember(Value = "CreepNotFound")]
        CreepNotFound,
        [EnumMember(Value = "NotEnoughRights")]
        NotEnoughRights,
        [EnumMember(Value = "StatNotFound")]
        StatNotFound,
        [EnumMember(Value = "EntityNotFound")]
        EntityNotFound
    }
}