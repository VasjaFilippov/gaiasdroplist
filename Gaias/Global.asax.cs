﻿using System;
using System.Drawing;
using System.Linq;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Gaias.Db;
using Gaias.Db.Entities;

namespace Gaias
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var rnd = new Random();

            using (var db = new Context())
            {
                if (!db.Items.Any())
                {
                    var colors = new[]
                    {
                        "gray",
                        "black",
                        "green",
                        "blue",
                        "red",
                    };

                    var stats = new Stat[15];
                    var itemTypes = new ItemType[20];
                    var itemColors = new ItemColor[5];
                    var items = new Item[5000];

                    for (var i = 0; i < 15; i++)
                    {
                        var stat = new Stat
                        {
                            IsMainStat = i < 3,
                            Name = $"Test stat {i}",
                        };

                        db.Stats.Add(stat);
                        stats[i] = stat;

                        if (!stat.IsMainStat)
                        {
                            db.StatDependancies.Add(new StatDependancy
                            {
                                Affected = stat,
                                Base = stats[i%3],
                            });
                        }
                    }

                    for (var i = 0; i < 20; i++)
                    {
                        var it = new ItemType { Name = $"Test item type {i}", };

                        db.ItemTypes.Add(it);
                        itemTypes[i] = it;

                        if (i >= 10)
                        {
                            it.Base = itemTypes[i%5];
                        }
                    }

                    for (var i = 0; i < 5; i++)
                    {
                        var ic = new ItemColor
                        {
                            Name = $"Color {i}",
                            Color = colors[i],
                        };

                        db.ItemColors.Add(ic);
                        itemColors[i] = ic;
                    }

                    for (var i = 0; i < 5000; i++)
                    {
                        var type = itemTypes[rnd.Next(0, 20)];
                        var item = new Item
                        {
                            Type = type,
                            Name = $"Test item {i}",
                            Color = itemColors[i%5],
                        };

                        items[i] = item;
                        db.Items.Add(item);
                        db.ItemBonuses.Add(GenerateBonus(rnd, item, stats[rnd.Next(0, 15)]));
                        db.ItemBonuses.Add(GenerateBonus(rnd, item, stats[rnd.Next(0, 15)]));
                        db.ItemBonuses.Add(GenerateBonus(rnd, item, stats[rnd.Next(0, 15)]));
                    }

                    for (var i = 0; i < 1000; i++)
                    {
                        var creep = new Creep
                        {
                            Name = $"Creep {i}",
                            Level = i%20 + 1,
                        };

                        db.Creeps.Add(creep);

                        for (var j = i*5; j < i*5 + 5; j++)
                        {
                            var drop = new CreepDrop
                            {
                                Creep = creep,
                                Item = items[j],
                            };

                            db.CreepDrops.Add(drop);
                        }
                    }

                    db.SaveChanges();
                }
            }
        }

        private static ItemBonus GenerateBonus(Random rnd, Item item, Stat stat)
        {
            var bonus = new ObjectStat
            {
                Stat = stat,
                Value = (float)(Math.Floor(rnd.NextDouble() * 9 + 1)),
            };
            
            return new ItemBonus
            {
                Item = item,
                Bonus = bonus,
            };
        }
    }
}
