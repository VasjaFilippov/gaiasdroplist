namespace Gaias.Db.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCreepStats : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Bonuses", newName: "ObjectStats");
            CreateTable(
                "dbo.CreepStats",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Creep_Id = c.Long(),
                        Stat_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Creeps", t => t.Creep_Id)
                .ForeignKey("dbo.ObjectStats", t => t.Stat_Id)
                .Index(t => t.Creep_Id)
                .Index(t => t.Stat_Id);
            
            DropColumn("dbo.Creeps", "MinDamage");
            DropColumn("dbo.Creeps", "MaxDamage");
            DropColumn("dbo.Creeps", "Armor");
            DropColumn("dbo.Creeps", "Health");
            DropColumn("dbo.Creeps", "Mana");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Creeps", "Mana", c => c.Single(nullable: false));
            AddColumn("dbo.Creeps", "Health", c => c.Single(nullable: false));
            AddColumn("dbo.Creeps", "Armor", c => c.Single(nullable: false));
            AddColumn("dbo.Creeps", "MaxDamage", c => c.Single(nullable: false));
            AddColumn("dbo.Creeps", "MinDamage", c => c.Single(nullable: false));
            DropForeignKey("dbo.CreepStats", "Stat_Id", "dbo.ObjectStats");
            DropForeignKey("dbo.CreepStats", "Creep_Id", "dbo.Creeps");
            DropIndex("dbo.CreepStats", new[] { "Stat_Id" });
            DropIndex("dbo.CreepStats", new[] { "Creep_Id" });
            DropTable("dbo.CreepStats");
            RenameTable(name: "dbo.ObjectStats", newName: "Bonuses");
        }
    }
}
