namespace Gaias.Db.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bonuses",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Value = c.Single(nullable: false),
                        Stat_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Stats", t => t.Stat_Id)
                .Index(t => t.Stat_Id);
            
            CreateTable(
                "dbo.Stats",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IsMainStat = c.Boolean(nullable: false),
                        Name = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name);
            
            CreateTable(
                "dbo.Drops",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Creep_Id = c.Long(nullable: false),
                        Item_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Creeps", t => t.Creep_Id, cascadeDelete: true)
                .ForeignKey("dbo.Items", t => t.Item_Id, cascadeDelete: true)
                .Index(t => t.Creep_Id)
                .Index(t => t.Item_Id);
            
            CreateTable(
                "dbo.Creeps",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Level = c.Int(nullable: false),
                        MinDamage = c.Single(nullable: false),
                        MaxDamage = c.Single(nullable: false),
                        Armor = c.Single(nullable: false),
                        Health = c.Single(nullable: false),
                        Mana = c.Single(nullable: false),
                        Name = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AddedBy = c.String(),
                        Name = c.String(maxLength: 256),
                        Color_Id = c.Long(),
                        Type_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ItemColors", t => t.Color_Id)
                .ForeignKey("dbo.ItemTypes", t => t.Type_Id)
                .Index(t => t.Name)
                .Index(t => t.Color_Id)
                .Index(t => t.Type_Id);
            
            CreateTable(
                "dbo.ItemBonuses",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Bonus_Id = c.Long(nullable: false),
                        Item_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bonuses", t => t.Bonus_Id, cascadeDelete: true)
                .ForeignKey("dbo.Items", t => t.Item_Id, cascadeDelete: true)
                .Index(t => t.Bonus_Id)
                .Index(t => t.Item_Id);
            
            CreateTable(
                "dbo.ItemColors",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Color = c.String(),
                        Name = c.String(maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name);
            
            CreateTable(
                "dbo.ItemTypes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        Base_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ItemTypes", t => t.Base_Id)
                .Index(t => t.Name)
                .Index(t => t.Base_Id);
            
            CreateTable(
                "dbo.Classes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        Base_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classes", t => t.Base_Id)
                .Index(t => t.Name)
                .Index(t => t.Base_Id);
            
            CreateTable(
                "dbo.StatDependancies",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Affected_Id = c.Long(nullable: false),
                        Base_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Stats", t => t.Affected_Id)
                .ForeignKey("dbo.Stats", t => t.Base_Id)
                .Index(t => t.Affected_Id)
                .Index(t => t.Base_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StatDependancies", "Base_Id", "dbo.Stats");
            DropForeignKey("dbo.StatDependancies", "Affected_Id", "dbo.Stats");
            DropForeignKey("dbo.Classes", "Base_Id", "dbo.Classes");
            DropForeignKey("dbo.Drops", "Item_Id", "dbo.Items");
            DropForeignKey("dbo.Items", "Type_Id", "dbo.ItemTypes");
            DropForeignKey("dbo.ItemTypes", "Base_Id", "dbo.ItemTypes");
            DropForeignKey("dbo.Items", "Color_Id", "dbo.ItemColors");
            DropForeignKey("dbo.ItemBonuses", "Item_Id", "dbo.Items");
            DropForeignKey("dbo.ItemBonuses", "Bonus_Id", "dbo.Bonuses");
            DropForeignKey("dbo.Drops", "Creep_Id", "dbo.Creeps");
            DropForeignKey("dbo.Bonuses", "Stat_Id", "dbo.Stats");
            DropIndex("dbo.StatDependancies", new[] { "Base_Id" });
            DropIndex("dbo.StatDependancies", new[] { "Affected_Id" });
            DropIndex("dbo.Classes", new[] { "Base_Id" });
            DropIndex("dbo.Classes", new[] { "Name" });
            DropIndex("dbo.ItemTypes", new[] { "Base_Id" });
            DropIndex("dbo.ItemTypes", new[] { "Name" });
            DropIndex("dbo.ItemColors", new[] { "Name" });
            DropIndex("dbo.ItemBonuses", new[] { "Item_Id" });
            DropIndex("dbo.ItemBonuses", new[] { "Bonus_Id" });
            DropIndex("dbo.Items", new[] { "Type_Id" });
            DropIndex("dbo.Items", new[] { "Color_Id" });
            DropIndex("dbo.Items", new[] { "Name" });
            DropIndex("dbo.Creeps", new[] { "Name" });
            DropIndex("dbo.Drops", new[] { "Item_Id" });
            DropIndex("dbo.Drops", new[] { "Creep_Id" });
            DropIndex("dbo.Stats", new[] { "Name" });
            DropIndex("dbo.Bonuses", new[] { "Stat_Id" });
            DropTable("dbo.StatDependancies");
            DropTable("dbo.Classes");
            DropTable("dbo.ItemTypes");
            DropTable("dbo.ItemColors");
            DropTable("dbo.ItemBonuses");
            DropTable("dbo.Items");
            DropTable("dbo.Creeps");
            DropTable("dbo.Drops");
            DropTable("dbo.Stats");
            DropTable("dbo.Bonuses");
        }
    }
}
