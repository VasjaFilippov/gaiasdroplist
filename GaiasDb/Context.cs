﻿using System.Data.Entity;
using Gaias.Db.Entities;

namespace Gaias.Db
{
    public class Context : DbContext
    {
        public DbSet<Item> Items { get; set; }
        public DbSet<ItemType> ItemTypes { get; set; }
        public DbSet<ItemBonus> ItemBonuses { get; set; }
        public DbSet<ObjectStat> ObjectStats { get; set; }
        public DbSet<GaiasClass> GaiasClasses { get; set; }
        public DbSet<Stat> Stats { get; set; }
        public DbSet<StatDependancy> StatDependancies { get; set; } 
        public DbSet<Creep> Creeps { get; set; }
        public DbSet<CreepDrop> CreepDrops { get; set; }
        public DbSet<ItemColor> ItemColors { get; set; } 
        public DbSet<CreepStat> CreepStats { get; set; } 

        public Context()
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.ValidateOnSaveEnabled = false;
            Configuration.AutoDetectChangesEnabled = false;
        }
    }
}
