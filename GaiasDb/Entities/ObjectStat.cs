﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Gaias.Db.Entities
{
    [Table("ObjectStats")]
    public class ObjectStat : Entity
    {
        public Stat Stat { get; set; }
        public float Value { get; set; }
    }
}