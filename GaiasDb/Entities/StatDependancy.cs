﻿using System.ComponentModel.DataAnnotations;

namespace Gaias.Db.Entities
{
    public class StatDependancy : Entity
    {
        [Required]
        public Stat Base { get; set; }
        [Required]
        public Stat Affected { get; set; }
    }
}