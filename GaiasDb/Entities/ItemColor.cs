﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Gaias.Db.Entities
{
    [Table("ItemColors")]
    public class ItemColor : GaiasObject
    {
         public string Color { get; set; } 
    }
}