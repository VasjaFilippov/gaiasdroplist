﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gaias.Db.Entities
{
    [Table("Items")]
    public class Item : GaiasObject
    {
        public string AddedBy { get; set; }
        public ItemType Type { get; set; }
        public ItemColor Color { get; set; }
        public virtual List<ItemBonus> Bonuses { get; set; }
        public virtual List<CreepDrop> Drops { get; set; }
    }
}