﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gaias.Db.Entities
{
    [Table("Creeps")]
    public class Creep : GaiasObject
    {
        public int Level { get; set; }
        public virtual List<CreepStat> Stats { get; set; }
        public virtual List<CreepDrop> Drops { get; set; }
    }
}