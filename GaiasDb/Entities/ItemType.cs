﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Gaias.Db.Entities
{
    [Table("ItemTypes")]
    public class ItemType : GaiasObject
    {
         public ItemType Base { get; set; }
    }
}