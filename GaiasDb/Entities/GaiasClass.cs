﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Gaias.Db.Entities
{
    [Table("Classes")]
    public class GaiasClass : GaiasObject
    {
         public GaiasClass Base { get; set; }
    }
}