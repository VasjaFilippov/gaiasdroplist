﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Gaias.Db.Entities
{
    [Table("Stats")]
    public class Stat : GaiasObject
    {
         public bool IsMainStat { get; set; }
    }
}