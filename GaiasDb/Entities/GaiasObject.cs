﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gaias.Db.Entities
{
    public class GaiasObject : Entity
    {
        [Index, StringLength(256)]
        public string Name { get; set; }
    }
}