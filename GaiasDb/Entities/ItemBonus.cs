﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gaias.Db.Entities
{
    [Table("ItemBonuses")]
    public class ItemBonus : Entity
    {
        [Required]
        public Item Item { get; set; }
        [Required]
        public ObjectStat Bonus { get; set; }
    }
}