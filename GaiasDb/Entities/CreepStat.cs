﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Gaias.Db.Entities
{
    [Table("CreepStats")]
    public class CreepStat : Entity
    {
        public Creep Creep { get; set; }
        public ObjectStat Stat { get; set; }
    }
}