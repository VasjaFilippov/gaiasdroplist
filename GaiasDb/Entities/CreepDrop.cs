﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gaias.Db.Entities
{
    [Table("Drops")]
    public class CreepDrop : Entity
    {
        [Required]
        public Creep Creep { get; set; }
        [Required]
        public Item Item { get; set; }
    }
}